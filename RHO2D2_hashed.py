'''
Contract FCI wave function to density matrices.  

return object that contains 1 and 2 particle density matrices
 

'''
from input import *
import time
import BasisBuilder as BB
import global_fun as gf
import Hamiltonian as Ham
import Diagonalize as DD
import numpy as np
from scipy.sparse import csr_matrix
import sys
import math
import cPickle as pickle

class FCI2RDM:

    def __init__(self, Diag, Basis, Na, Nb, L):

        #EVALUATE D1a by <\Psi|a_{i}^{t}a_{j}|\Psi>

        #BUILD NONZERO WF COEFFICIENTS
        M = 2**L
        gs = Diag#Diag.eigenvectors[:,0]
        CI = []
        It = []

        #CREATE DICTIONARY--i.e. HASHTABLE TO SPEEDUP LOOKUPS
        strings = {}
        stringsr = {}
        for i in range(len(gs)):

            lax = Basis[i] % M
            lbx = Basis[i] / M
            _, ket_a = gf.DectoBin_2(Na, lax, L)
            _, ket_b = gf.DectoBin_2(Nb, lbx, L)
            strings[lbx] = ket_b
            strings[lax] = ket_a
            stringsr[tuple(ket_a)] = lax
            stringsr[tuple(ket_b)] = lbx


            if np.abs(gs[i]) > 1.0E-15:
                CI.append(gs[i])
                It.append(Basis[i])
        
        #D1a = self.BuildD1a(CI, It, strings)
        #D2aa = self.BuildD2aa(CI, It, strings, L)
        #D2ab = self.BuildD2ab(CI,It, strings, L)
        D2aa, Uaa = self.BuildD2aa_2(Diag,Basis, strings,stringsr, L)
        D2ab, Uab = self.BuildD2ab_2(Diag,Basis, strings,stringsr, L)

        self.Uaa = Uaa
        self.Uab = Uab
        self.D2ab = D2ab
        self.D2aa = D2aa

    def BuildD2ab_2(self,CI, It,strings,stringsr ,L):

        dim = int(L*L) #BASIS i < j no such thing as
        M = 2**L
        D2ab = np.zeros((dim,dim))
        #BUILD BASIS FOR D2ab
        D2abBas = {}
        cnt = 0
        for r in range(L):
            for z in range(L):
                D2abBas[cnt] = (r,z)
                cnt += 1

        LD_FCI = CI.shape[0]
        outer_coeff = np.outer(CI,CI).reshape(1,-1, order='C')
        U_row = []
        U_col = []
        U_dat = []

        #U = np.zeros( (LD_FCI**2, dim**2) )


        for i in xrange(D2ab.shape[0]):
            for j in xrange(i,D2ab.shape[1]):

                for k in xrange(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    ket_a = strings[Iket_a]
                    ket_b = strings[Iket_b]

                    ket_a_t = list(ket_a)
                    ket_b_t = list(ket_b)
                    
                    goodK1, sign1 = gf.Killsign(D2abBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2abBas[j][1], ket_b_t)
                    goodC1, sign3 = gf.Createsign(D2abBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2abBas[i][1], ket_b_t)
                    if goodK1 and goodK2 and goodC1 and goodC2:
                        Ia_ket_t = stringsr[tuple(ket_a_t)]
                        Ib_ket_t = stringsr[tuple(ket_b_t)]
                        Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D2ab[i,j] += CI[k]*CI[l]
                                #U[LD_FCI*k + l, i*dim+j] = 1.0
                                U_row.append(LD_FCI*k + l)
                                U_col.append(i*dim+j)
                                U_dat.append(1.0)
                            else:
                                D2ab[i,j] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                D2ab[j,i] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                #U[LD_FCI*k + l, i*dim+j] =\
                                #1.0*sign1*sign2*sign3*sign4
                                #U[LD_FCI*k + l, j*dim+i] =\
                                #1.0*sign1*sign2*sign3*sign4
                                U_row.append(LD_FCI*k + l)
                                U_col.append(i*dim+j)
                                U_dat.append(1.0*sign1*sign2*sign3*sign4)
                                U_row.append(LD_FCI*k + l)
                                U_col.append(j*dim+i)
                                U_dat.append(1.0*sign1*sign2*sign3*sign4)
 
                        except ValueError:
                            pass


        
        #td2 = np.dot(outer_coeff, U)
        #td2 = td2.reshape(dim,dim)
        #print td2.shape
        #print D2ab.shape
        #w,v = np.linalg.eigh(D2ab)
        #ww,vv = np.linalg.eigh(td2)
        #for xx in range(len(w)): print "%f,%f,%f" % (w[xx],ww[xx], w[xx] -
        #        ww[xx])
        #sys.exit()
        U = csr_matrix( (U_dat, (U_row,U_col)), shape=(LD_FCI**2,dim**2), dtype='int')
        return D2ab, U



    def BuildD2ab(self,CI, It,strings, L):

        dim = int(L*L) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2ab = np.zeros((dim,dim))

        #BUILD BASIS FOR D2aa
        D2abBas = {}
        cnt = 0
        for r in range(L):
            for z in range(L):
                #print "%i,%i" % (r+1,z+1)
                D2abBas[cnt] = (r,z)
                cnt += 1

        for i in xrange(D2ab.shape[0]):
            for j in xrange(i,D2ab.shape[1]):

                for k in xrange(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    #_, ket_a = gf.DectoBin(Na, Iket_a)
                    #_, ket_b = gf.DectoBin(Nb, Iket_b)
                    ket_a = strings[Iket_a]
                    ket_b = strings[Iket_b]

                    ket_a_t = list(ket_a)
                    ket_b_t = list(ket_b)
                    
                    goodK1, sign1 = gf.Killsign(D2abBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2abBas[j][1], ket_b_t)
                    goodC1, sign3 = gf.Createsign(D2abBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2abBas[i][1], ket_b_t)
                    if goodK1 and goodK2 and goodC1 and goodC2:
                        Ia_ket_t = gf.ItfromVec(ket_a_t)
                        Ib_ket_t = gf.ItfromVec(ket_b_t)
                        Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D2ab[i,j] += CI[k]*CI[l]
                            else:
                                D2ab[i,j] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                D2ab[j,i] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                #print D2ab[i,j]
                        except ValueError:
                            pass


        return D2ab

    def BuildD2aa_2(self,eigvec, Basis, strings,stringsr, L):

        dim = int(L*(L - 1)/2.) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2aa = np.zeros((dim,dim))
        D2bas = {}
        cnt = 0
        for xx in range(L):
            for yy in range(xx+1,L):
                D2bas[cnt] = (xx,yy)
                cnt += 1

        LD_FCI = eigvec.shape[0]
        outer_coeff = np.outer(eigvec,eigvec).reshape(1,-1, order='C') #C[i]*C[j] 
        tout = csr_matrix(outer_coeff)
        #print "%f GB outer_coeff" % (outer_coeff.nbytes / 1000. / 1000. / 1000.)
        #print "%f GB U mat" % (outer_coeff.nbytes*(dim**2) / 1000. / 1000. / 1000.)
        #U = np.zeros( (LD_FCI**2, dim**2),dtype='int' ) #ROWS = outer_coeff SELECTION,
        #        dim IS SELECTION FOR D2 MATRIX ELEMENT.
        U_row = []
        U_col = []
        U_dat = []
        #BUILD BASIS FOR D2aa
        D2aaBas = {}
        cnt = 0
        for r in xrange(L):
            for z in xrange(r+1,L):
                D2aaBas[cnt] = (r,z)
                cnt += 1
      
        FCI_COEFF_LIST = []
        for i in xrange(D2aa.shape[0]):
            for j in xrange(i,D2aa.shape[1]):
                #print "c%ic%ik%ik%i" % (D2aaBas[i][0], D2aaBas[i][1],
                #        D2aaBas[j][1], D2aaBas[j][0])
                for k in xrange(eigvec.shape[0]):

                    Iket_a = Basis[k] % M
                    Iket_b = Basis[k] / M
                    
                    ket_a = strings[Iket_a]
                    ket_b = strings[Iket_b]
                    ket_a_t = list(ket_a)
                    goodK1, sign1 = gf.Killsign(D2aaBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2aaBas[j][1], ket_a_t)
                    goodC1, sign3 = gf.Createsign(D2aaBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2aaBas[i][1], ket_a_t)
 

                    if goodK1 and goodK2 and goodC1 and goodC2:
                        Ia_ket_t = stringsr[tuple(ket_a_t)]
                        Iket_t = (2**L)*Iket_b + Ia_ket_t
                        
                        try:
                            l = Basis.index(Iket_t)
                            if i == j:
                                D2aa[i,j] += eigvec[k]*eigvec[l]  
                                FCI_COEFF_LIST.append((k,l))
                                #U[LD_FCI*k + l, i*dim+j] = 1.0           
                                U_row.append(LD_FCI*k + l)
                                U_col.append(i*dim+j)
                                U_dat.append(1.0)
                                #print D2aa[i,j]
                                #print np.dot(outer_coeff, U)[0,i*dim+j]
                                #time.sleep(1)
                            else:
                                D2aa[i,j] += \
                                eigvec[k]*sign1*sign2*sign3*sign4*eigvec[l]
                                D2aa[j,i] += \
                                eigvec[k]*sign1*sign2*sign3*sign4*eigvec[l]
                                FCI_COEFF_LIST.append((k,l))
                                #U[LD_FCI*k + l, i*dim+j] = 1.0*sign1*sign2*sign3*sign4   
                                #U[LD_FCI*k + l, j*dim+i] = 1.0*sign1*sign2*sign3*sign4   
                                U_row.append(LD_FCI*k + l)
                                U_col.append(i*dim+j)
                                U_dat.append(1.0*sign1*sign2*sign3*sign4)
                                U_row.append(LD_FCI*k + l)
                                U_col.append(j*dim+i)
                                U_dat.append(1.0*sign1*sign2*sign3*sign4)
                                #print D2aa[i,j]
                                #print np.dot(outer_coeff, U)[0, i*dim+j]
                                #time.sleep(1)
                        except ValueError:
                            pass
                #print FCI_COEFF_LIST

        #print outer_coeff.shape
        #print U.shape
        #tdt2a = np.dot(outer_coeff, U)
        #td2 = np.zeros(D2aa.shape)
        #td2 = tdt2a.reshape(dim,dim)
        ##td2 = (td2 + td2)/2.
        ##for xx in range(td2.shape[0]):
        ##    for yy in range(xx,td2.shape[1]):
        ##        if xx == yy:
        ##            td2[xx,yy] = tdt2a[0,xx*dim + yy]
        ##        else:
        ##            td2[xx,yy] = tdt2a[0,xx*dim + yy]
        ##            td2[yy,xx] = tdt2a[0,xx*dim + yy]
 
        #print td2.shape
        #print D2aa.shape
        #print np.linalg.norm(td2-D2aa)
        #w,v = np.linalg.eigh(D2aa)
        #ww,vv = np.linalg.eigh(td2)
        #for xx in range(len(w)): print "%f,%f,%f" %  (w[xx],ww[xx],
        #        w[xx]-ww[xx])
        #
        #sys.exit()
        U = csr_matrix( (U_dat, (U_row,U_col)), shape=(LD_FCI**2,dim**2),dtype='int')
        return D2aa, U


    def BuildD2aa(self,CI, It, strings, L):

        dim = int(L*(L - 1)/2.) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2aa = np.zeros((dim,dim))

        #BUILD BASIS FOR D2aa
        D2aaBas = {}
        cnt = 0
        for r in xrange(L):
            for z in xrange(r+1,L):
                #print "%i,%i" % (r+1,z+1)
                D2aaBas[cnt] = (r,z)
                cnt += 1
        
        for i in xrange(D2aa.shape[0]):
            for j in xrange(i,D2aa.shape[1]):

                for k in xrange(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    
                    ket_a = strings[Iket_a]
                    ket_b = strings[Iket_b]
                    #_, ket_a = gf.DectoBin(Na, Iket_a)
                    #_, ket_b = gf.DectoBin(Nb, Iket_b)
                    ket_a_t = list(ket_a)
                    #print "<%i,%i|%i%i>" % (D2aaBas[j][0], D2aaBas[j][1], 
                    #        D2aaBas[i][0], D2aaBas[i][1])
                    goodK1, sign1 = gf.Killsign(D2aaBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2aaBas[j][1], ket_a_t)
                    goodC1, sign3 = gf.Createsign(D2aaBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2aaBas[i][1], ket_a_t)
  
                    if goodK1 and goodK2 and goodC1 and goodC2:
                        Ia_ket_t = gf.ItfromVec(ket_a_t)
                        Iket_t = (2**L)*Iket_b + Ia_ket_t
                        
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D2aa[i,j] += CI[k]*CI[l]  
                            else:
                                D2aa[i,j] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                D2aa[j,i] += \
                                CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                #print D2aa[i,j]
                        except ValueError:
                            pass

                        #for l in xrange(len(CI)):
                        #    if Iket_t == It[l]:
                        #        if i == j:
                        #            D2aa[i,j] += CI[k]*CI[l]  
                        #        else:
                        #            D2aa[i,j] += \
                        #            CI[k]*sign1*sign2*sign3*sign4*CI[l]
                        #            D2aa[j,i] += \
                        #            CI[k]*sign1*sign2*sign3*sign4*CI[l]
                        #            #print D2aa[i,j]

        return D2aa

   
    def BuildD1a(self, CI, It, strings ):

        #PRELIMINARIES FOR EXTRACTING STRINGS AS USUAL
        M = 2**L

        #EACH KILLER CREATOR ACTS AT EACH NONZERO COEFFICIENT
        #FOR DIAGONAL ELEMENTS JUST CHECK IF 1 IS THERE. AND IF BETA STRING IS
        #THE SAME

        #FOR OFF DIAGONAL ELEMENTS WE NEED TO CREATE KILL, PUT INTO CANONICAL
        #ORDERING, AND CHECK BETA STRING.  #ALPHA BLOCK DOES NOT TALK TO BETA
        #BLOCK
        D1a = np.zeros((L,L))
        for i in xrange(L):
            for j in xrange(i,L): #UPPER TRIANGLE
                #print "%i,%i" % (i,j)
                
                #LOOP OVER ENTIRE CI VECTOR KILL AT EACH
                for k in xrange(len(CI)):

                    lax = It[k] % M
                    lbx = It[k] / M

                    ket_a = strings[lax]
                    ket_b = strings[lbx]
                    #_, ket_a = gf.DectoBin(Na, lax)
                    #_, ket_b = gf.DectoBin(Nb, lbx)

                    #ket = ''.join(str(x) for x in ket_a)
                    #ket += '|' + ''.join(str(x) for x in ket_b)

                    #print "%s" % ket

                    ket_a_t = list(ket_a)
                    goodK, sign1 = gf.Killsign( j, ket_a_t)
                    goodC, sign2 = gf.Createsign( i, ket_a_t)
                    if goodK and goodC:
                        Ia_ket_t = gf.ItfromVec(ket_a_t)
                        Iket_t = (2**L)*lbx + Ia_ket_t
                        try:
                            l = It.index(Iket_t)
                            if i == j:
                                D1a[i,j] += CI[k]*CI[k]
                            else:
                                D1a[i,j] += CI[k]*sign1*sign2*CI[l]
                                D1a[j,i] += CI[k]*sign1*sign2*CI[l]

                        except ValueError:
                            pass
                            #print Iket_t
                            #print "lbx = %i" %lbx
                            #print "Ia_ket_t = %i" %Ia_ket_t
                            #print strings[lbx]
                            #print strings[Ia_ket_t]

                        #for l in xrange(len(CI)): #OKAY CHECK BRA's NOW FOR
                        #    #EQUIVALENCY
                        #    #print "%i =? %i" % (It[l], Iket_t) 
                        #    if Iket_t == It[l]: #WE FOUND A MATCHING STRING
                        #        #print "YES"                  
                        #        if i == j:
                        #            D1a[i,j] += CI[k]*CI[k]
                        #        else:
                        #            D1a[i,j] += CI[k]*sign1*sign2*CI[l]
                        #            D1a[j,i] += CI[k]*sign1*sign2*CI[l]

        return D1a

if __name__=="__main__":

    #Debug we need to Build Basis
    #Build Hamiltonian
    #Diagonalize
    #then call this routine.
    
    fid = open('test.log','w')
    #BUILD BASIS
    Basis_states = BB.Basis(4**L,Na,Nb, fid)
    #BUILD HAMILTONIAN
    HH = Ham.Hamiltonian(Basis_states, fid)
    #DIAGONALIZE
    Diag = DD.Diagonalize(HH.Ham_Mat, Basis_states, fid) 
    
    FCI2RDM(Diag,Basis_states, fid)

    fid.close()
