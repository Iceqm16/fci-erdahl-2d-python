'''
Extract ground state and first excited state from fci.out in active directory.
Look at the energy gap as a function of maximal eigenvalue in the
2-RDM/Eigenvalue distribution.
'''


import os
import sys
import numpy 
from pylab import *
import unittest
rc('font', family="Times New Roman")



class CollectS0S1:

    def __init__(self,):

        dirs = filter(os.path.isdir, os.listdir('./'))
        GS = []
        ES = []
        alpha = [] #LIST OF TUPLES

        dirt = {}
        for d in dirs:
            dirt[int(d.split('r')[1])] = d

        print dirs
        print dirt.keys()
        for dd in dirt.keys():

            os.chdir(dirt[dd])
            Eigs = []
            print os.getcwd()
            fid = open('fci.out','r')
            line = fid.readline()


            while('EIGENVALUES' not in line): #ADVANCE TO eigenvalue line
                if 'alpha_t' in line:
                    alpha.append((float(line.split('=')[1]),
                            float(fid.readline().split('=')[1])))
                    print alpha[-1]
                line = fid.readline()

            while('NONZERO' not in line):
                line = fid.readline()
                #print line.strip()
                try:
                    Eigs.append(float(line.strip()))
                except ValueError:
                    break

            GS.append(Eigs[0])
            ES.append(Eigs[1])
            fid.close()
            os.chdir('../')

        os.chdir('../')
        self.GS = GS
        self.ES = ES
        self.alpha = alpha


class PlotStates:

    def __init__(self,GS, ES, alpha):

        axes = figure().add_subplot(111)    
        plot(range(len(ES)),ES,'bs-',linewidth=1.25, label='Excited State')
        plot(range(len(GS)),GS,'ro-',linewidth=1.25,label='Ground State')
        #plot(alpha,ES,'bs-',linewidth=1.25, label='Excited State')
        #plot(alpha,GS,'ro-',linewidth=1.25,label='Ground State')
 
        xlabel(r'$\alpha$',fontsize=24)
        ylabel(r'$E$',fontsize=24)
        tick_params(labelsize=10)
        tight_layout()
        legend(loc='upper right', frameon=False, prop={'size':18})
        a=axes.get_xticks().tolist()
        #a[-1] = 37
        #a[4] = 19
        print a
        print len(ES)
        #print map(int,a)
        #sys.exit()
        #a = map(lambda x: "(%2.2f,%2.2f)" % (alpha[x-1][0], alpha[x][1]),
        #       map(int,a))
        #print a
        #axes.set_xticklabels(a)
        #xticks(rotation=70)
        xlim([0,38])
        savefig('test.pdf',format='PDF')
        show()



if __name__=="__main__":

    if len(sys.argv) != 2:
        print "Only one active directoyr required."
        sys.exit()

    print "moving to active directory"
    os.chdir(sys.argv[1])

    S0S1 = CollectS0S1()
    PlotStates(S0S1.GS, S0S1.ES, S0S1.alpha)
