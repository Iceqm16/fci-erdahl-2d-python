'''
Run the FCI program


1) Build basis
2) Call Hamiltonian
3) diagonalize
4) print eigenvalues

'''
import sys
import os
import numpy as np
sys.path.insert(0,'%s'% os.getcwd())
from input import *
import BasisBuilder as BB
import Hamiltonian as Ham
import Diagonalize as DD
import Nrep as NR
import FCI2RDM as C2D
import time

if __name__=="__main__":

    start_time = time.time()
    fid = open("fci.out",'w')
    #PRINT BANNER
    str1 = "\n\n\tFULL CONFIGURATION INTERACTION\n\tBY NICHOLAS C. RUBIN\n\n"
    print str1
    fid.write(str1)

    #STATE NUMBER
    nn = 4**L

    #ENFORCE SQUARE LATTICE
    #if np.sqrt(L)%2 != 0:
    #    print "L must be perfect square"
    #    sys.exit()


    #PRINT VARIABLES
    #try:
    #    print "\t\tU = %f" % U
    #    fid.write("\t\tU = %f\n" % U)
    #except:
    #    pass

    if ConservedN:
        fid.write("\t\tConservedN = %s\n" % ConservedN)
        print "\t\tNa = %f" % Na
        print "\t\tNb = %f" % Nb
        fid.write("\t\tNa = %f\n" % Na)
        fid.write("\t\tNb = %f\n" % Nb)

    if NaNb:
        fid.write("\t\tNaNb = %s\n" % NaNb)
        fid.write("\t\tTotal N = %s\n" % N)
    try:
        print "\t\tt = %f" % t
    except:
        pass

    print "\t\tL = %f\n" % L
    fid.write("\t\tL = %f\n" % L)
    print "\t\talpha_t = %f\n" % alpha_t
    fid.write("\t\talpha_t = %f\n" % alpha_t)
    print "\t\talpha_e = %f\n" % alpha_e
    fid.write("\t\talpha_e = %f\n\n" % alpha_e)




    #BUILD BASIS
    Basis_states = BB.Basis(nn,Na,Nb, fid)

    #BUILD HAMILTONIAN
    HH = Ham.Hamiltonian(Basis_states, fid)

    #DIAGONALIZE
    #Diag = DD.Diagonalize(HH.Ham_Mat, Basis_states, fid) 

    ##ROUTINE FOR GETTING D2 AND D1 FROM FCI RUN!
    #RDMs = C2D.FCI2RDM(Diag,Basis_states, fid)

    ##CALCULATE N-DENSITY MATRIX, EVALUATE BETA
    #NR.Nrep(Diag,HH, fid) 

    print "\n\n\n\t\tTOTAL TIME = %f\n" % (time.time() - start_time)
    print "\tSUCCESS! GOODBYE...\n"
    fid.write("\n\n\t\tTOTAL TIME = %f\n" % (time.time() - start_time) )
    fid.write("\t\tSUCCESS! GOODBYE....\n")
    fid.close()
