'''
Contract FCI wave function to 2 density matrix.  

 

'''
from input import *
import time
import BasisBuilder as BB
import global_fun as gf
import Hamiltonian as Ham
import Diagonalize as DD
import numpy as np
from scipy.sparse import csr_matrix
import sys
from scipy.linalg import block_diag

class FCI2RDM:

    def __init__(self, CI, It):

        #EVALUATE D1a by <\Psi|a_{i}^{t}a_{j}|\Psi>
        print "Building RDMs"
        #BUILD NONZERO WF COEFFICIENTS
        M = 2**L 
        print "Building 1-RDM alpha"
        D1a = self.BuildD1a(CI, It)
        print "Building 2-RDM aa"
        D2aa = self.BuildD2aa(CI, It)
        print "Building 2-RDM ab"
        D2ab = self.BuildD2ab(CI,It)

        self.D1a = D1a
        self.D2aa = D2aa
        self.D2ab = D2ab
        D2 = block_diag(D2aa,D2aa,D2ab)
        self.D2 = D2

    def BuildD2ab(self,CI, It):

        dim = int(L*L) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2ab = np.zeros((dim,dim))

        #BUILD BASIS FOR D2aa
        D2abBas = {}
        cnt = 0
        for r in range(L):
            for z in range(L):
                #print "%i,%i" % (r+1,z+1)
                D2abBas[cnt] = (r,z)
                cnt += 1

        for i in range(D2ab.shape[0]):
            for j in range(i,D2ab.shape[1]):

                for k in range(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    _, ket_a = gf.DectoBin(Na, Iket_a)
                    _, ket_b = gf.DectoBin(Nb, Iket_b)

                    ket_a_t = list(ket_a)
                    ket_b_t = list(ket_b)
                    
                    goodK1, sign1 = gf.Killsign(D2abBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2abBas[j][1], ket_b_t)
                    goodC1, sign3 = gf.Createsign(D2abBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2abBas[i][1], ket_b_t)
                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket_t

                    if goodK1 and goodK2 and goodC1 and goodC2:

                        for l in range(len(CI)):
                            if Iket_t == It[l]:
                                if i == j:
                                    D2ab[i,j] += CI[k]*CI[l]
                                else:
                                    D2ab[i,j] += \
                                    CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                    D2ab[j,i] += \
                                    CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                    #print D2ab[i,j]

        #print D2ab
        #w,v = np.linalg.eigh(D2ab)
        #print w
        return D2ab



    def BuildD2aa(self,CI, It):

        dim = int(L*(L - 1)/2.) #BASIS i < j no such thing as
        #a_{i}^{dagg}a_{i}^{dagg}
        M = 2**L
        D2aa = np.zeros((dim,dim))

        #BUILD BASIS FOR D2aa
        D2aaBas = {}
        cnt = 0
        for r in range(L):
            for z in range(r+1,L):
                print "%i,%i" % (r+1,z+1)
                D2aaBas[cnt] = (r,z)
                cnt += 1
        
        for i in range(D2aa.shape[0]):
            for j in range(i,D2aa.shape[1]):

                for k in range(len(CI)):

                    Iket_a = It[k] % M
                    Iket_b = It[k] / M
                    _, ket_a = gf.DectoBin(Na, Iket_a)
                    _, ket_b = gf.DectoBin(Nb, Iket_b)

                    ket_a_t = list(ket_a)
                    goodK1, sign1 = gf.Killsign(D2aaBas[j][0], ket_a_t)
                    goodK2, sign2 = gf.Killsign(D2aaBas[j][1], ket_a_t)
                    goodC1, sign3 = gf.Createsign(D2aaBas[i][0], ket_a_t)
                    goodC2, sign4 = gf.Createsign(D2aaBas[i][1], ket_a_t)
                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Iket_b + Ia_ket_t
                    if goodK1 and goodK2 and goodC1 and goodC2:

                        for l in range(len(CI)):
                            if Iket_t == It[l]:
                                if i == j:
                                    D2aa[i,j] += CI[k]*CI[l]
                                else:
                                    D2aa[i,j] += \
                                    CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                    D2aa[j,i] += \
                                    CI[k]*sign1*sign2*sign3*sign4*CI[l]
                                    #print D2aa[i,j]
        return D2aa

    def BuildD1b(self, CI, It ):

        #PRELIMINARIES FOR EXTRACTING STRINGS AS USUAL
        M = 2**L

        #EACH KILLER CREATOR ACTS AT EACH NONZERO COEFFICIENT
        #FOR DIAGONAL ELEMENTS JUST CHECK IF 1 IS THERE. AND IF BETA STRING IS
        #THE SAME

        #FOR OFF DIAGONAL ELEMENTS WE NEED TO CREATE KILL, PUT INTO CANONICAL
        #ORDERING, AND CHECK BETA STRING.  #ALPHA BLOCK DOES NOT TALK TO BETA
        #BLOCK
        D1b = np.zeros((L,L))
        for i in range(L):
            for j in range(i,L): #UPPER TRIANGLE
                #print "%i,%i" % (i,j)
                
                #LOOP OVER ENTIRE CI VECTOR KILL AT EACH
                for k in range(len(CI)):

                    lax = It[k] % M
                    lbx = It[k] / M

                    _, ket_a = gf.DectoBin(Na, lax)
                    _, ket_b = gf.DectoBin(Nb, lbx)

                    ket = ''.join(str(x) for x in ket_a)
                    ket += '|' + ''.join(str(x) for x in ket_b)

                    #print "%s" % ket

                    ket_b_t = list(ket_b)
                    goodK, sign1 = gf.Killsign( j, ket_b_t)
                    goodC, sign2 = gf.Createsign( i, ket_b_t)
                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + lax
                    if goodK and goodC:
                        #print "goodK goodC"
                        for l in range(len(CI)): #OKAY CHECK BRA's NOW FOR
                            #EQUIVALENCY
                            #print "%i =? %i" % (It[l], Iket_t) 
                            if Iket_t == It[l]: #WE FOUND A MATCHING STRING
                                #print "YES"                  
                                if i == j:
                                    D1b[i,j] += CI[k]*CI[k]
                                else:
                                    D1b[i,j] += CI[k]*sign1*sign2*CI[l]
                                    D1b[j,i] += CI[k]*sign1*sign2*CI[l]

        return D1b


    def BuildD1a(self, CI, It ):

        #PRELIMINARIES FOR EXTRACTING STRINGS AS USUAL
        M = 2**L

        #EACH KILLER CREATOR ACTS AT EACH NONZERO COEFFICIENT
        #FOR DIAGONAL ELEMENTS JUST CHECK IF 1 IS THERE. AND IF BETA STRING IS
        #THE SAME

        #FOR OFF DIAGONAL ELEMENTS WE NEED TO CREATE KILL, PUT INTO CANONICAL
        #ORDERING, AND CHECK BETA STRING.  #ALPHA BLOCK DOES NOT TALK TO BETA
        #BLOCK
        D1a = np.zeros((L,L))
        for i in range(L):
            for j in range(i,L): #UPPER TRIANGLE
                print "%i,%i" % (i,j)
                
                #LOOP OVER ENTIRE CI VECTOR KILL AT EACH
                for k in range(len(CI)):

                    lax = It[k] % M
                    lbx = It[k] / M

                    _, ket_a = gf.DectoBin(Na, lax)
                    _, ket_b = gf.DectoBin(Nb, lbx)

                    ket = ''.join(str(x) for x in ket_a)
                    ket += '|' + ''.join(str(x) for x in ket_b)

                    #print "%s" % ket
                    #sys.exit()

                    ket_a_t = list(ket_a)
                    goodK, sign1 = gf.Killsign( j, ket_a_t)
                    goodC, sign2 = gf.Createsign( i, ket_a_t)
                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*lbx + Ia_ket_t
                    if goodK and goodC:
                        #print "goodK goodC"
                        for l in range(len(CI)): #OKAY CHECK BRA's NOW FOR
                            #EQUIVALENCY
                            #print "%i =? %i" % (It[l], Iket_t) 
                            if Iket_t == It[l]: #WE FOUND A MATCHING STRING
                                #print "YES"                  
                                if i == j:
                                    D1a[i,j] += CI[k]*CI[k]
                                else:
                                    D1a[i,j] += CI[k]*sign1*sign2*CI[l]
                                    D1a[j,i] += CI[k]*sign1*sign2*CI[l]

        return D1a

if __name__=="__main__":

    #Debug we need to Build Basis
    #Build Hamiltonian
    #Diagonalize
    #then call this routine.
    
    fid = open('test.log','w')
    #BUILD BASIS
    Basis_states = BB.Basis(4**L,Na,Nb, fid)
    #BUILD HAMILTONIAN
    HH = Ham.Hamiltonian(Basis_states, fid)
    #DIAGONALIZE
    Diag = DD.Diagonalize(HH.Ham_Mat, Basis_states, fid) 
    
    FCI2RDM(Diag,Basis_states, fid)

    fid.close()
