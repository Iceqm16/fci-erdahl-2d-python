from pylab import *
import os
import sys
import unittest
rc('font', family="Times New Roman")

if __name__=="__main__":


    alphat = [-1]*10
    alphae = list(np.linspace(-1,1,10))
    betat = []
    betae = []

    betat.append(0)
    betae.append(1)

    os.chdir(sys.argv[1])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat.append(float(line[0]))
                betae.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')

    os.chdir(sys.argv[2])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat.append(float(line[0]))
                betae.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')
 
    os.chdir('../')
    
    os.chdir(sys.argv[3])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat.append(float(line[0]))
                betae.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')

    betat.append(0)
    betae.append(1)


    print alphae
    print betat
    print betae


    plot(betat, betae, 'bs-', linewidth=1.25, label=r'$|\Lambda| = 4 \; \langle n \rangle = 1$', markersize=8)


    xlabel(r'$\beta_{T}$', fontsize=24)
    ylabel(r'$\beta_{E}$', fontsize=24)

    #ylabel(r'$|\langle \hat{n}_{i,\alpha}\hat{n}_{i+R,\beta}\rangle - \langle\hat{n}_{\alpha}\rangle\langle\hat{n}_{\beta}\rangle|$',
    #        fontsize=24)
    #ylim([-14,0])
    tick_params(labelsize=20)
    #xlabel(r'$U$', fontsize=28)
    
    tight_layout()
    legend(loc='upper left', frameon=False, prop={'size':20})

    print os.getcwd()
    savefig('test.pdf', format='PDF')
    show()
       
