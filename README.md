FCI on Erdahl's Superconducting Model 
by Nicholas C. Rubin
------

FCI on a superconducting model model on square lattice.  Also contains
rudimentary contraction of FCI density to a 2-RDM.  input should be fairly
self-explanatory.

paper [ref](http://pubs.acs.org/doi/abs/10.1021/jp5130266)

