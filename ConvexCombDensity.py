'''
Plot the representable region then plot beta(thermal) on that region.  Use the
files T2.ham and E2.ham to get those particular components of the Hamiltonian
matrix for the AGP state we have data for.

1) plot boundary.
'''
import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
import scipy.misc as spm
from input import *
import global_fun as gf
plt.rc('font', family="Times New Roman")
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
import time



def CollectBeta(tdir,thome):

    #INITIALIZE STORAGE LISTS
    betae = []
    betat = []
    betae_ex1 = []
    betat_ex1 = []
    betae_ex2 = []
    betat_ex2 = []

    #MOVE TO DIRECTORY AND COLLECT DATA
    os.chdir(tdir)
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        try:
            dirt[int(d.split('r')[1])] = d
        except:
            dirt[int(d.split('a')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        with open('fci.out','r') as fid:
            line = fid.readline()
            while 'SUCCESS' not in line:

                if 'BETA\'S' in line:
                    line = fid.readline()
                    line = line.split('\t')
                    betat.append(float(line[0]))
                    betae.append(float(line[1]))
                    break
                line = fid.readline()

            while 'SUCCESS' in line:
                if 'BETA\'S 2' in line:
                    line = fid.readline()
                    line = line.split('\t')
                    betat_ex1.append(float(line[0]))
                    betae_ex1.append(float(line[1]))
                    break
                line = fid.readline()

            while 'SUCCESS' in line:
                if 'BETA\'S 3' in line:
                    line = fid.readline()
                    line = line.split('\t')
                    betat_ex2.append(float(line[0]))
                    betae_ex2.append(float(line[1]))
                    break
                line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir(thome)
    return betat, betae, betat_ex1, betae_ex1, betat_ex2, betae_ex2  

def ExtractVecs(tfile,num_states):


    CI_list = []
    CIcoeffs = []
    tIT = []
    with open(tfile,'r') as fid:

        orders = map(lambda x: [x,x+1], range(num_states))
        for s1, s2 in orders:
            #ADVANCE TO s1
            line = ""
            while("NONZERO COEFFICIENTS FOR STATE = %i"%s1 not in line):
                line = fid.readline()
            #print line
            #print "Collecting %i density" % s1           
            #GET COEFFS AND CONSTRUCT DENSITY
            tCIcoeff, IT, tCIcoeff_list = ReturnCIIT(fid,s1,s2)
            CI_list.append(tCIcoeff)
            CIcoeffs.append(tCIcoeff_list)
            tIT.append(IT)

    fid.close()
    return CI_list, tIT, CIcoeffs
      
def ReturnCIIT(fid,state1,state2):

    It = []
    tcnt = 0
    
    if int(os.getcwd().split('/')[-1].split('r')[1]) < int(L/2):
        max_cnt = int(round(spm.comb(L,
            int(os.getcwd().split('/')[-1].split('r')[1] ) )) )
    else:
        max_cnt = int(round(spm.comb(L,int(L/2))))

    CIcoeff = np.zeros(max_cnt)
    CIcoeff_list = []
    line = ""
    while(tcnt < max_cnt):#"NONZERO COEFFICIENTS FOR STATE = %i"%state2 not in line):
        try:
            line = fid.readline()
            line = line.split('\t')
            CIcoeff[int(line[2])] =  float(line[0]) #.append(float(line[0]))#[
            CIcoeff_list.append(float(line[0]))#[
            state = map(lambda x: int(x), line[1].split("|")[0] )
            Iket = gf.ItfromVec(state)
            It.append(int(2**L)*Iket + Iket)
            tcnt = int(line[2])+1
        except:
            print "exiting a value error"
            print line
            print state
            print It[-1]

            break

    return CIcoeff, It, CIcoeff_list



def FindNumStates(tfile):

    with open(tfile,'r') as fid:

        cnt = 0
        line = fid.readline()
        while("SUCCESS! GOODBYE..." not in line):
            if "STATE =" in line:
                cnt += 1
                line = fid.readline()
            else:
                line = fid.readline()

        fid.close()
    return cnt

def ExtractEigs(tfile,num_states):

    with open(tfile,'r') as fid:

        line = fid.readline()
        while("EIGENVALUES" not in line):
            line = fid.readline()

        Eigs = []
        cnt = 0
        while (cnt < num_states):
            Eigs.append(float(fid.readline()))
            cnt += 1

        return Eigs

def ConstructThermal(rho_list,eig_list):

    #CHECKS MAKE SURE ALL RHO HAVE SAME DIMENSION
    for ridx in range(len(rho_list)):
        print rho_list[ridx].shape
        if rho_list[ridx].shape != rho_list[0].shape:
            print "rho not of same size"
            print ridx
            sys.exit()

    #INVERSE TEMPERATURE RANGE 0,5
    kt_inv = np.linspace(5,0,40)
    #MAKE PARTITION FUNCTIONS
    Z_list = []
    for bidx in range(len(kt_inv)):
        ztmp = 0
        for eidx in range(len(eig_list)):
            ztmp += np.exp(-1*kt_inv[bidx]*eig_list[eidx])
        Z_list.append(ztmp)

    #Z_LIST CONTAINS ALL PARTITION FUNCTIONS WITH SPECIFIC BETA VALUES
    #NOW WE MUST CONSTRUCT THE THERMAL DENSITY MATRIX AT EACH INVERSE
    #TEMPERATURE. LOOP THROUGH PARTITION FUNCTIONS/KT_INV AND THEN LOOP THROUGH
    #RHO LIST EVALUTING THE FOLLOWIN 
    #RHO(BETA) = EXP[-1*BETA*E_{I}]/Z[BETA]*RHO_{I}
    ThermalD_list = []

    for bidx in range(len(kt_inv)):
        print "Building Thermal D Num %i/%i beta = %f" %\
        (bidx,len(kt_inv),kt_inv[bidx])
        tden = np.zeros(rho_list[0].shape)
        
        for didx in range(len(rho_list)):
            weight = np.exp(-1*kt_inv[bidx]*eig_list[didx])/Z_list[bidx]
            print "w_{%i} = %f" % (didx, weight)
            tden = np.add(weight*rho_list[didx],tden)

        ThermalD_list.append(tden) 

        #fidt = open("ThermalD%i.d2"%bidx,'w')
        #fidt.write("beta = {: 4.10E}\n".format(kt_inv[bidx]))
        #for xx in range(tden.shape[0]):
        #    for yy in range(tden.shape[1]):
        #        print tden[xx,yy]
        #        fidt.write("{: 4.10E}\n".format(tden[xx,yy]))
        #fidt.close()
    return ThermalD_list, kt_inv



def CollectThermalBeta(tdir, thome):

    os.chdir(tdir)
    tfile = os.getcwd() + '/fci.out'
    cnt = FindNumStates(tfile) 
    CI_list, It_list, CIcoeff_list = ExtractVecs(tfile,cnt)
    eig_list = ExtractEigs(tfile,cnt)
    Density = []
    for xx in range(len(It_list)):
        Density.append( np.outer(CI_list[xx],CI_list[xx] ) )
    #ThermList, inv_temp = ConstructThermal(Density, eig_list)  
    Density = map(csr_matrix, Density)
    
    #COLLECT HAMILTONAINS E2 T2
    E2 = pd.read_csv('./E2.ham')
    T2 = pd.read_csv('./T2.ham')
    E2 = csr_matrix( (E2['val'], (E2['row'],E2['col']) ),
            shape=(Density[0].shape) )
    T2 = csr_matrix( (T2['val'], (T2['row'],T2['col']) ),
            shape=(Density[0].shape) )

    betat = []
    betae = []
    for xx in range(len(Density)):
        [tbetat,tbetae] = [ np.sum(Density[xx].dot(T2).diagonal())/L ,
                np.sum(Density[xx].dot(E2).diagonal() )/L ]
        betat.append(tbetat)
        betae.append(tbetae)


    os.chdir(thome)

    return Density, betat, betae, CI_list, It_list, CIcoeff_list

def GetPairRDMs_2( rfiles, lfiles, thome):


    rDGS = []
    rD2 = []
    lDGS = []
    lD2 = []
    T2_list = []
    E2_list = []
    #COLLECT GROUND STATE DENSITY MATRICES 
    os.chdir(rfiles)
    dirs = filter(os.path.isdir,os.listdir('./'))
    tdirt = {}
    for xx in dirs:
        tdirt[int(xx.split('r')[1])] = xx

    for d in tdirt.keys()[1:]:
        os.chdir(tdirt[d])
        print os.getcwd()
        cnt = 2#FindNumStates('./fci.out') 
        CI_list, tIT, CIcoeffs = ExtractVecs('fci.out',cnt) 
        fidt = open('./fci.out','r')
        D2ab_elements = []
        while 1:
            line = fidt.readline()
            if "D2ab ELEMENTS" in line:
                break
        while 1:
            try:
                line = fidt.readline()
                D2ab_elements.append(float(line))
            except:
                break
        fidt.close()


        #MEMORY REQUIREMENTS FOR ANALYSIS ARE TOO HIGH. ONLY STORE VECTORS AND
        #GENERATE DENSITY MATRIX ON FLY
        rDGS.append( CI_list[0]) #csr_matrix(np.outer(CI_list[0],CI_list[0])) ) # for p in range(len(CI_list))]  )
        print "CI list"
        print len(CI_list[0])
        print type(CI_list[0])
        rD2.append( np.array(D2ab_elements).reshape( (int(np.sqrt(len(D2ab_elements))),-1) ) )
        print os.listdir('./')
        print os.getcwd()
        print "Current d = %i" % d
        ld = len(CI_list[0])
        #if d < 9:
        E2 = pd.read_csv('./E2.ham')
        T2 = pd.read_csv('./T2.ham')
        E2 = csr_matrix( (E2['val'], (E2['row'],E2['col']) ),
                shape=( (ld,ld) ) ) #.todense()
        T2 = csr_matrix( (T2['val'], (T2['row'],T2['col']) ),
                shape=( ld,ld) ) #.todense()
        T2_list.append(T2)
        E2_list.append(E2)

        os.chdir('../')

    os.chdir(thome)
    os.chdir(lfiles)
    dirs = filter(os.path.isdir,os.listdir('./'))
    tdirt = {}
    for xx in dirs:
        tdirt[int(xx.split('r')[1])] = xx

    for d in tdirt.keys()[1:]:
        os.chdir(tdirt[d])
        print "Current d = %i" %d
        cnt = 2#FindNumStates('./fci.out') 
        CI_list, tIT, CIcoeffs = ExtractVecs('fci.out',cnt) 
        fidt = open('./fci.out','r')
        D2ab_elements = []
        while 1:
            line = fidt.readline()
            if "D2ab ELEMENTS" in line:
                break
        while 1:
            try:
                line = fidt.readline()
                D2ab_elements.append(float(line))
            except:
                break

        fidt.close()

        #MEMORY REQUIREMENTS FOR ANALYSIS ARE TOO HIGH. ONLY STORE VECTORS AND
        #GENERATE DENSITY MATRIX ON FLY
        lDGS.append( CI_list[0])#np.outer(CI_list[0], CI_list[0]) )# for p in range(len(CI_list))] )
        lD2.append(np.array(D2ab_elements).reshape( (int(np.sqrt(len(D2ab_elements))),-1) ) )

        os.chdir('../')

    return rDGS, lDGS, T2_list, E2_list, rD2, lD2



if __name__=="__main__":

    #COLLECT BETA's FOR BOUNDARY
    if len(sys.argv) != 3:
        print "NUM INPUTS = %i" % len(sys.argv)
        print "NUM INPUTS == 4"
        sys.exit()

    thome = os.getcwd()
    betat, betae, betat_ex1, betae_ex1, betat_ex2, betae_ex2  = CollectBeta(sys.argv[1], os.getcwd())
    betat2, betae2, betat2_ex1, betae2_ex1, betat2_ex2, betae2_ex2  = CollectBeta(sys.argv[2], os.getcwd())

    #np.save("betat",np.array(betat + betat2))
    #np.save("betae", np.array(betae+betae2))
    #sys.exit()

    #GET PAIRS STARTING FROM 1 to -2
    #rDGS, lDGS, T2_list, E2_list, rD2, lD2 = GetPairRDMs(sys.argv[1], sys.argv[2], os.getcwd())
    rDGS, lDGS, T2_list, E2_list, rD2, lD2 = GetPairRDMs_2(sys.argv[1], sys.argv[2], os.getcwd())

    #NOTE: NOW WE HAVE STORED FEWER HAMILTONIANS AND THE DENSITY MATRICES IN
    #VECTOR FORM.  ADAPT CODE BELOW THIS POINT TO THIS STORAGE METHOD.  MORE
    #COMPUTATION IS TRADED FOR MORE MEMORY REQUIREMENTS


    #EACH ELEMENT IS A GROUND STATE RHO
    os.chdir(thome)

    g = list(np.linspace(0,1,20))
    cvx_list =[]
    cvx_D2_list = []
    betat_set = []
    betae_set = []
    lambda_set = []
    betat_set.append([0]*len(g)) #SET STARTS ON THE LEFT
    betae_set.append([2.0]*len(g))
    lambda_set.append([0]*len(g))

    #FOR EACH RHO PAIR TAKE CONVEX COMBINATION
    print "Calculating RHO"
    start_time = time.time()
    #print len(rDGS)
    #print len(T2_list)
    #for didx in range(len(rDGS)):
    #    print didx
    #    tmpMat = csr_matrix(np.outer(rDGS[didx], rDGS[didx]))
    #    #if didx < int(L/2)-1:
    #    betat_set.append(np.sum(tmpMat.dot(T2_list[didx]).diagonal() )/L )
    #    betae_set.append(np.sum(tmpMat.dot(E2_list[didx]).diagonal() )/L )
    #    #else:
    #    #    print T2_list[-1].shape
    #    #    print T2_list[len(T2_list)-1].shape
    #    #    print tmpMat.shape
    #    #    betat_set.append(np.sum(tmpMat.dot(T2_list[-1]).diagonal() )/L )
    #    #    betae_set.append(np.sum(tmpMat.dot(E2_list[-1]).diagonal() )/L )
 
    #    tmpMat = None
    #print len(lDGS)
    #for didx in range(len(lDGS)):
    #    print didx
    #    tmpMat = csr_matrix(np.outer(lDGS[didx], lDGS[didx]))
    #    #if didx < int(L/2)-1:
    #    betat_set.append(np.sum(tmpMat.dot(T2_list[didx]).diagonal() )/L )
    #    betae_set.append(np.sum(tmpMat.dot(E2_list[didx]).diagonal() )/L )
    #    #else:
    #    #    betat_set.append(np.sum(tmpMat.dot(T2_list[-1]).diagonal() )/L )
    #    #    betae_set.append(np.sum(tmpMat.dot(E2_list[-1]).diagonal() )/L )
    #    tmpMat = None

    #    cvx_D2_list.append([p*lD2[didx] + (1.-p)*rD2[didx] for p in g])


    #np.save("betat_set",betat_set)
    #np.save("betae_set",betae_set)
    #sys.exit()


    for didx in range(len(rDGS)):
        tldgs = csr_matrix(np.outer(lDGS[didx], lDGS[didx]))
        trdgs = csr_matrix(np.outer(rDGS[didx], rDGS[didx]))

        tbetat_set = []
        tbetae_set = []
        tlambda_set = []
        print "Converted two rhos to csr in %f s" % (time.time() - start_time)
        print "Starting Iteration %i/%i" % (didx,len(rDGS))
        for p in g:
            tmpMat = csr_matrix(p*tldgs + (1.-p)*trdgs)
            #if didx < int(L/2):
            tbetat_set.append(np.sum(tmpMat.dot(T2_list[didx]).diagonal() )/L ) 
            tbetae_set.append(np.sum(tmpMat.dot(E2_list[didx]).diagonal() )/L ) 
            #else:
            #    tbetat_set.append(np.sum(tmpMat.dot(T2_list[-1]).diagonal() )/L ) 
            #    tbetae_set.append(np.sum(tmpMat.dot(E2_list[-1]).diagonal() )/L ) 
            tmpMat = None
        betat_set.append(tbetat_set)
        betae_set.append(tbetae_set)

        #tlist = [p*tldgs + (1.-p)*trdgs for p in g]
        #cvx_list.append(tlist)
        cvx_D2_list.append([p*lD2[didx] + (1.-p)*rD2[didx] for p in g])
        tldgs = None
        trdgs = None

        #tlist = [p*lDGS[didx] + (1.-p)*rDGS[didx] for p in g]
        #cvx_list.append(tlist)
        #cvx_D2_list.append([p*lD2[didx] + (1.-p)*rD2[didx] for p in g])


    #betat_set = []
    #betae_set = []
    #lambda_set = []
    #betat_set.append([0]*len(cvx_list[0]))
    #betae_set.append([1]*len(cvx_list[0]))
    #lambda_set.append([0]*len(cvx_list[0]))
    for xx in range(len(cvx_D2_list)):
        #betat_set.append([np.trace(np.dot(T2_list[xx], cvx_list[xx][i]))/L for i in
        #    range(len(cvx_list[xx]))])
        #betae_set.append([np.trace(np.dot(E2_list[xx], cvx_list[xx][i]))/L for i in
        #    range(len(cvx_list[xx]))])
        tlambda = []
        for kk in range(len(cvx_D2_list[xx])):
            w, v = np.linalg.eigh(cvx_D2_list[xx][kk])
            tlambda.append(w[-1])
        lambda_set.append(tlambda)


    #betat_set.append([0]*len(cvx_list[0]))
    #betae_set.append([-1]*len(cvx_list[0]))
    #lambda_set.append(tlambda)


    #PLOT POINTS
    #fig, ax1 = plt.subplots()
    #ax1.plot(betat, betae,'bo-',mfc='None',mec='b',markersize=8,linewidth=1.25,
    #label=r'$^{N}D$')
    #ax1.plot(betat2,
    #        betae2,'bo-',mfc='None',mec='b',markersize=8,linewidth=1.25)

    #for item in range(len(betat_set)):
    #    if len(betat_set) - item == 1:
    #        ax1.plot(betat_set[item],
    #                betae_set[item],'g^-',mfc='None',mec='g',markersize=8,linewidth=1.25,
    #                label=r'CVX $^{N}D$')
    #    else:
    #        ax1.plot(betat_set[item], betae_set[item],'g^-',mfc='None',mec='g',markersize=8,linewidth=1.25)
    #    

    #ax1.set_xlabel(r'$\beta_{T}$',fontsize=24)
    #ax1.set_ylabel(r'$\beta_{E}$',fontsize=24)
    #ax1.tick_params(labelsize=18)
    #fig.tight_layout()
    #ax1.legend(loc='upper left',frameon=False,prop={'size':18})
    #fig.savefig('test.pdf',format='PDF')

    #fig2 = plt.figure()
    #ax = fig2.gca(projection='3d')
    #SET X, Y, Z
    X = np.zeros( (len(betat_set), len(betat_set[0]) ) )
    Y = np.zeros( (len(betat_set), len(betat_set[0]) ) )
    Z = np.zeros( (len(betat_set), len(betat_set[0]) ) )
    for xx in range(X.shape[0]):
        for yy in range(X.shape[1]):
            X[xx,yy] = betat_set[xx][yy]
            Y[xx,yy] = betae_set[xx][yy]
            Z[xx,yy] = lambda_set[xx][yy]


    np.save("X",X)
    np.save("Y",Y)
    np.save("Z",Z)
    sys.exit()

    ax.plot_surface(X,Y,Z,rstride=1,cstride=1,alpha=0.3)# rstride=8, cstride=8, alpha=0.3)
    cset = ax.contourf(X, Y, Z, zdir='z', offset=0, cmap=cm.coolwarm)
    cset = ax.contourf(X, Y, Z, zdir='x', offset=-1.6, cmap=cm.coolwarm)
    cset = ax.contourf(X, Y, Z, zdir='y', offset=1, cmap=cm.coolwarm)
    #cset = plt.contourf(X, Y, Z, offset=1, cmap=cm.coolwarm)

    #plt.xlabel(r'$\beta_{T}$',fontsize=24)
    #plt.xlim(-1.6,1.6)
    #plt.ylabel(r'$\beta_{E}$',fontsize=24)
    ##plt.colorbar(cset)
    #plt.ylim(-1,1)

    ax.set_xlabel(r'$\beta_{T}$',fontsize=24)
    ax.set_xlim(-1.6,1.6)
    ax.set_ylabel(r'$\beta_{E}$',fontsize=24)
    ax.set_ylim(-1,1)
    ax.set_zlabel(r'$\lambda_{\mathrm{max}}$',fontsize=24)
    ax.set_zlim(0,2.6)
    fig2.savefig('test.pdf',format='PDF')
    plt.show()
