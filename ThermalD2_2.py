'''
Plot the representable region then plot beta(thermal) on that region.  Use the
files T2.ham and E2.ham to get those particular components of the Hamiltonian
matrix for the AGP state we have data for.

1) plot boundary.
'''
import sys
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix
import scipy.misc as spm
from input import *
import global_fun as gf
import RHO2D2 as r2d
plt.rc('font', family="Times New Roman")
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
import re
import multiprocessing as mp



def CollectBeta(tdir,thome):

    #INITIALIZE STORAGE LISTS
    betae = []
    betat = []
    betae_ex1 = []
    betat_ex1 = []
    betae_ex2 = []
    betat_ex2 = []

    #MOVE TO DIRECTORY AND COLLECT DATA
    os.chdir(tdir)
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        try:
            dirt[int(d.split('r')[1])] = d
        except:
            dirt[int(d.split('a')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        with open('fci.out','r') as fid:
            line = fid.readline()
            while 'SUCCESS' not in line:

                if 'BETA\'S' in line:
                    line = fid.readline()
                    line = line.split('\t')
                    betat.append(float(line[0]))
                    betae.append(float(line[1]))
                    break
                line = fid.readline()

            while 'SUCCESS' in line:
                if 'BETA\'S 2' in line:
                    line = fid.readline()
                    line = line.split('\t')
                    betat_ex1.append(float(line[0]))
                    betae_ex1.append(float(line[1]))
                    break
                line = fid.readline()

            while 'SUCCESS' in line:
                if 'BETA\'S 3' in line:
                    line = fid.readline()
                    line = line.split('\t')
                    betat_ex2.append(float(line[0]))
                    betae_ex2.append(float(line[1]))
                    break
                line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir(thome)
    return betat, betae, betat_ex1, betae_ex1, betat_ex2, betae_ex2  

def ExtractVecs(tfile,num_states):


    CI_list = []
    CIcoeffs = []
    tIT = []
    with open(tfile,'r') as fid:

        orders = map(lambda x: [x,x+1], range(num_states))
        for s1, s2 in orders:
            #ADVANCE TO s1
            line = ""
            while("NONZERO COEFFICIENTS FOR STATE = %i"%s1 not in line):
                line = fid.readline()
            #print line
            #print "Collecting %i density" % s1           
            #GET COEFFS AND CONSTRUCT DENSITY
            tCIcoeff, IT, tCIcoeff_list = ReturnCIIT(fid,s1,s2)
            CI_list.append(tCIcoeff)
            CIcoeffs.append(tCIcoeff_list)
            tIT.append(IT)

    fid.close()
    return CI_list, tIT, CIcoeffs
      
def ReturnCIIT(fid,state1,state2):

    It = []
    tcnt = 0
    
    if int(os.getcwd().split('/')[-1].split('r')[1]) < int(L/2):
        max_cnt = int(round(spm.comb(L,
            int(os.getcwd().split('/')[-1].split('r')[1] ) )) )
    else:
        max_cnt = int(round(spm.comb(L,int(L/2))))

    CIcoeff = np.zeros(max_cnt)
    CIcoeff_list = []
    line = ""
    while(tcnt < max_cnt):#"NONZERO COEFFICIENTS FOR STATE = %i"%state2 not in line):
        try:
            line = fid.readline()
            line = line.split('\t')
            CIcoeff[int(line[2])] =  float(line[0]) #.append(float(line[0]))#[
            CIcoeff_list.append(float(line[0]))#[
            state = map(lambda x: int(x), line[1].split("|")[0] )
            Iket = gf.ItfromVec(state)
            It.append(int(2**L)*Iket + Iket)
            tcnt = int(line[2])+1
        except:
            print "exiting a value error"
            print line
            print state
            #print It[-1]

            break

    return CIcoeff, It, CIcoeff_list



def FindNumStates(tfile):

    with open(tfile,'r') as fid:

        cnt = 0
        line = fid.readline()
        while("SUCCESS! GOODBYE..." not in line):
            if "STATE =" in line:
                cnt += 1
                line = fid.readline()
            else:
                line = fid.readline()

        fid.close()
    return cnt

def ExtractEigs(tfile,num_states=10000000):

    with open(tfile,'r') as fid:

        line = fid.readline()
        while("EIGENVALUES" not in line):
            line = fid.readline()

        Eigs = []
        cnt = 0 
        while (cnt < num_states):
            try:
                Eigs.append(float(fid.readline()))
                cnt += 1
            except:
                break

        return Eigs




def GetParams(tfile):

    fid = open(tfile,'r')
    text = fid.read()
    L = int(re.findall('(L = \d)',text)[0].split('=')[1])
    Na = int(re.findall('(Na = \d)',text)[0].split('=')[1])
    Nb = int(re.findall('(Nb = \d)',text)[0].split('=')[1])
    fid.close()

    return L, Na, Nb

def GetRho( tfiles,  thome):


    T2_list = []
    E2_list = []
    RDMs = []
    D2 = []
    Eig_list = []
    RHO = []
    #COLLECT GROUND STATE DENSITY MATRICES 
    os.chdir(tfiles)
    dirs = filter(os.path.isdir,os.listdir('./'))
    tdirt = {}
    for xx in dirs:
        tdirt[int(xx.split('r')[1])] = xx

    for d in tdirt.keys()[1:]:
        os.chdir(tdirt[d])
        print os.getcwd()
        ll, na, nb = GetParams('./input.py')
        tEigs = ExtractEigs('./fci.out')
        Eig_list.append( tEigs)
        #CI_list, tIT, CIcoeffs = ExtractVecs('fci.out',cnt) 
        CIvecs = np.load('./Eigenvectors.npy')

        #RDMs.append(  [r2d.FCI2RDM(CIcoeffs[p], tIT[p])  for p in range(len(CI_list)) ]  )
        try:
            RHO.append(  [CIvecs[:,p]  for p in range(CIvecs.shape[0]) ]  )
        except:
            RHO.append( CIvecs )

        D2.append( np.load('./D2ab.npy') )

        #CHECK TO SEE IF WE CAN CONTRACT RHO[0] to RDM[0]

        E2 = pd.read_csv('./E2.ham')
        T2 = pd.read_csv('./T2.ham')
        E2 = csr_matrix( (E2['val'], (E2['row'],E2['col']) ),
                shape=( CIvecs.shape ) )
        T2 = csr_matrix( (T2['val'], (T2['row'],T2['col']) ),
                shape=( CIvecs.shape ) )
        T2_list.append(T2)
        E2_list.append(E2)

        os.chdir('../')

    os.chdir(thome)

    return RHO, T2_list, E2_list, D2, Eig_list

def ConstructThermal(rho_list,eig_list):

    #CHECKS MAKE SURE ALL RHO HAVE SAME DIMENSION
    for ridx in range(len(rho_list)):
        print rho_list[ridx].shape
        if rho_list[ridx].shape != rho_list[0].shape:
            print "rho not of same size"
            print ridx
            sys.exit()

    #INVERSE TEMPERATURE RANGE 0,5
    kt_inv = np.linspace(5,0,20)
    #MAKE PARTITION FUNCTIONS
    Z_list = []
    for bidx in range(len(kt_inv)):
        ztmp = 0
        for eidx in range(len(eig_list)):
            ztmp += np.exp(-1*kt_inv[bidx]*eig_list[eidx])
        Z_list.append(ztmp)

    #Z_LIST CONTAINS ALL PARTITION FUNCTIONS WITH SPECIFIC BETA VALUES
    #NOW WE MUST CONSTRUCT THE THERMAL DENSITY MATRIX AT EACH INVERSE
    #TEMPERATURE. LOOP THROUGH PARTITION FUNCTIONS/KT_INV AND THEN LOOP THROUGH
    #RHO LIST EVALUTING THE FOLLOWIN 
    #RHO(BETA) = EXP[-1*BETA*E_{I}]/Z[BETA]*RHO_{I}
    ThermalD_list = []

    for bidx in range(len(kt_inv)):
        print "Building Thermal D Num %i/%i beta = %f" %\
        (bidx,len(kt_inv),kt_inv[bidx])
        tden = np.zeros(rho_list[0].shape)
        
        for didx in range(len(rho_list)):
            weight = np.exp(-1*kt_inv[bidx]*eig_list[didx])/Z_list[bidx]
            print "w_{%i} = %f" % (didx, weight)
            tden = np.add(weight*rho_list[didx],tden)

        ThermalD_list.append(tden) 

        #fidt = open("ThermalD%i.d2"%bidx,'w')
        #fidt.write("beta = {: 4.10E}\n".format(kt_inv[bidx]))
        #for xx in range(tden.shape[0]):
        #    for yy in range(tden.shape[1]):
        #        print tden[xx,yy]
        #        fidt.write("{: 4.10E}\n".format(tden[xx,yy]))
        #fidt.close()
    return ThermalD_list, kt_inv

def ConstructThermal_sparse(rho_list,eig_list):

    #CHECKS MAKE SURE ALL RHO HAVE SAME DIMENSION
    for ridx in range(len(rho_list)):
        print rho_list[ridx].shape
        if rho_list[ridx].shape != rho_list[0].shape:
            print "rho not of same size"
            print ridx
            sys.exit()

    #INVERSE TEMPERATURE RANGE 0,5
    kt_inv = np.linspace(5,0,20)
    #MAKE PARTITION FUNCTIONS
    Z_list = []
    for bidx in range(len(kt_inv)):
        ztmp = 0
        for eidx in range(len(eig_list)):
            ztmp += np.exp(-1*kt_inv[bidx]*eig_list[eidx])
        Z_list.append(ztmp)

    #Z_LIST CONTAINS ALL PARTITION FUNCTIONS WITH SPECIFIC BETA VALUES
    #NOW WE MUST CONSTRUCT THE THERMAL DENSITY MATRIX AT EACH INVERSE
    #TEMPERATURE. LOOP THROUGH PARTITION FUNCTIONS/KT_INV AND THEN LOOP THROUGH
    #RHO LIST EVALUTING THE FOLLOWIN 
    #RHO(BETA) = EXP[-1*BETA*E_{I}]/Z[BETA]*RHO_{I}

    #EVALUATE BETAT AND BETAE SO WE ARE NOT STORING SO MANY MATRICES AT ONCE

    ThermalD_list = []

    for bidx in range(len(kt_inv)):
        print "Building Thermal D Num %i/%i beta = %f" %\
        (bidx,len(kt_inv),kt_inv[bidx])
        tden = csr_matrix(np.zeros( (rho_list[0].shape[0], rho_list[0].shape[0]) ))
        
        for didx in range(len(rho_list)):
            weight = np.exp(-1*kt_inv[bidx]*eig_list[didx])/Z_list[bidx]
            print "w_{%i} = %f" % (didx, weight)
            tden = weight*csr_matrix(np.outer(rho_list[didx], rho_list[didx])) + tden

        ThermalD_list.append(tden) 

        #fidt = open("ThermalD%i.d2"%bidx,'w')
        #fidt.write("beta = {: 4.10E}\n".format(kt_inv[bidx]))
        #for xx in range(tden.shape[0]):
        #    for yy in range(tden.shape[1]):
        #        print tden[xx,yy]
        #        fidt.write("{: 4.10E}\n".format(tden[xx,yy]))
        #fidt.close()
    return ThermalD_list, kt_inv


def ConstructThermal_sparse_betas_star(input):
    #print "starting, ", input
    return ConstructThermal_sparse_betas(*input)

def ConstructThermal_sparse_betas(rho_list,eig_list, T2, E2):

    #CHECKS MAKE SURE ALL RHO HAVE SAME DIMENSION
    for ridx in range(len(rho_list)):
        #print rho_list[ridx].shape
        if rho_list[ridx].shape != rho_list[0].shape:
            print "rho not of same size"
            print ridx
            sys.exit()

    #INVERSE TEMPERATURE RANGE 0,5
    kt_inv = np.linspace(5,0,10)
    #MAKE PARTITION FUNCTIONS
    Z_list = []
    for bidx in range(len(kt_inv)):
        ztmp = 0
        for eidx in range(len(eig_list)):
            ztmp += np.exp(-1*kt_inv[bidx]*eig_list[eidx])
        Z_list.append(ztmp)

    #Z_LIST CONTAINS ALL PARTITION FUNCTIONS WITH SPECIFIC BETA VALUES
    #NOW WE MUST CONSTRUCT THE THERMAL DENSITY MATRIX AT EACH INVERSE
    #TEMPERATURE. LOOP THROUGH PARTITION FUNCTIONS/KT_INV AND THEN LOOP THROUGH
    #RHO LIST EVALUTING THE FOLLOWIN 
    #RHO(BETA) = EXP[-1*BETA*E_{I}]/Z[BETA]*RHO_{I}

    #EVALUATE BETAT AND BETAE SO WE ARE NOT STORING SO MANY MATRICES AT ONCE

    ThermalD_list = []
    betat = []
    betae = []
    lambda_max = []
    for bidx in range(len(kt_inv)):
        print "Building Thermal D Num %i/%i beta = %f" %\
        (bidx,len(kt_inv),kt_inv[bidx])
        tden = csr_matrix(np.zeros( (rho_list[0].shape[0], rho_list[0].shape[0]) ))
        
        for didx in range(len(rho_list)):
            weight = np.exp(-1*kt_inv[bidx]*eig_list[didx])/Z_list[bidx]
            print "w_{%i} = %f" % (didx, weight)
            tden = weight*csr_matrix(np.outer(rho_list[didx], rho_list[didx])) + tden


        #NOW WE HAVE THE THERMAL DENSITy tDEN WE CAN EVALUATE BETAT AND BETAE
        #AND NOT HAVE TO SAVE THE MATRIX
        betat.append(np.sum(tden.dot(T2).diagonal()) / L)
        betae.append(np.sum(tden.dot(E2).diagonal()) / L)
         

    return betat, betae, kt_inv




if __name__=="__main__":

    #COLLECT BETA's FOR BOUNDARY
    if len(sys.argv) != 2:
        print "NUM INPUTS = %i" % len(sys.argv)
        print "NUM INPUTS == 4"
        sys.exit()

    thome = os.getcwd()
    betat, betae, betat_ex1, betae_ex1, betat_ex2, betae_ex2  = CollectBeta(sys.argv[1], os.getcwd())


    RDMs_list, T2_list, E2_list, D2, Eig_list = GetRho(sys.argv[1], os.getcwd())

    os.chdir(thome)


    Therm_RDM = []
    betat_set = []
    betae_set = []

    #WE SHOULD USE MAP REDUCE HERE.  SINGLE CORE OVER THE LIST WILL TAKE TOO
    #LONG. USE pool multiprocessing TO EXECUTE FROM DICTIONARY BASE.
    #for kk in range(len(RDMs_list)):
    #    tbetat, tbetae, kt_inv = ConstructThermal_sparse_betas(RDMs_list[kk],
    #            Eig_list[kk], T2_list[kk], E2_list[kk])
    #    betat_set.append(tbetat)
    #    betae_set.append(tbetae)

    print "num cpu, ", mp.cpu_count()
    pool = mp.Pool(processes=5)#mp.cpu_count())  #ACCESSES NUMBER AVAILABLE  #processes=8)
    
    results = pool.map(ConstructThermal_sparse_betas_star,  [  (RDMs_list[kk], Eig_list[kk],
        T2_list[kk], E2_list[kk],)  for kk in range(len(RDMs_list)) ]  )
    

    for tbetat, tbetae, kt_inv in results:
        betat_set.append(tbetat)
        betae_set.append(tbetae)
        

    X = np.zeros( (len(betat_set), len(betat_set[0]) ) )
    Y = np.zeros( (len(betat_set), len(betat_set[0]) ) )
    for xx in range(X.shape[0]):
        for yy in range(X.shape[1]):
            X[xx,yy] = betat_set[xx][yy]
            Y[xx,yy] = betae_set[xx][yy]

    
    print os.getcwd()
    np.save("X_multicore",X)
    np.save("Y_multicore",Y)
    sys.exit()



