'''
Generate input files for superconducting Hamiltonian fci.
alphat and alphae are given 
'''

import os
import sys
import numpy as np

def writeFCIpbs(num):

    fid = open('FCIsub.pbs','w')

    fid.write("###PBS SUBMISSION SCRIPT\n\n")
    fid.write("#PBS -N %s\n" % ("FCI_run_%i"% r))
    fid.write("#PBS -e fci.e\n")
    fid.write("#PBS -o fci.o\n")
    fid.write("#PBS -l nodes=1:ppn=1\n\n")
    
    fid.write("module load epd\n\n")

    fid.write("SCR_PATH=\"/state/partition1/scr/nick\"\n\n")
    fid.write("if [ ! -e $SCR_PATH ]; then\nmkdir -p $SCR_PATH\nfi\n\n")

    fid.write("cd $PBS_O_WORKDIR\n\n")

    fid.write("python /home/nick/pyncr/ED/FCIrun.py\n")

    fid.close()

def writeInpy(at,ae,L,Na,Nb,CN):

    fid = open('input.py','w')

    fid.write("global L, Na, Nb\n")
    fid.write("L = %i\n" % L)
    fid.write("Na = %i\n" % Na)
    fid.write("Nb = %i\n" % Nb)
    fid.write("ConservedN = %s\n" % str(CN))
    fid.write("alpha_t = %f\n" % at)
    fid.write("alpha_e = %f\n" % ae)

if __name__=="__main__":

    os.chdir(sys.argv[1])

    alphat = -1
    alphae = np.linspace(-1,1,30)

    L = 6
    Na = 3
    Nb = 3
    CN = True

    for r in range(len(alphae)):

        os.mkdir('r%i'%r)

        os.chdir('r%i'%r)
        writeFCIpbs(r)
        writeInpy(alphat, alphae[r], L, Na, Nb, CN)

        os.chdir('../')
