from pylab import *
import os
import sys
import unittest
rc('font', family="Times New Roman")

if __name__=="__main__":

    thermo = [1,0]
    diffx = []
    diffy =[]

    for xx in map(lambda x: 2*x,range(3,100)):
        diffx.append((float(xx)/(xx-1)) - thermo[0])


    print diffx

    plot(map(lambda x: 2*x, range(3,100)), diffx, 'rs-', linewidth=1.25)

    show()
        
