from pylab import *
import os
import sys
import unittest
rc('font', family="Times New Roman")

if __name__=="__main__":


    betat1 = []
    betae1 = []

    betat1.append(0)
    betae1.append(1)

    betat_ex = []
    betae_ex = []
    betat_ex1 = []
    betae_ex1 = []

    os.chdir(sys.argv[1])
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        dirt[int(d.split('r')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat1.append(float(line[0]))
                betae1.append(float(line[1]))
                break

            line = fid.readline()
        

        print "finding Beta 2"

        while 'SUCCESS' not in line:
            if 'BETA\'S 2' in line:
                line = fid.readline()
                line = line.split('\t')
                betat_ex.append(float(line[0]))
                betae_ex.append(float(line[1]))
                break
            print line
            line = fid.readline()
        
        print "finding beta3"

        while 'SUCCESS' not in line:
          
            if 'BETA\'S 3' in line:
                line = fid.readline()
                line = line.split('\t')
                betat_ex1.append(float(line[0]))
                betae_ex1.append(float(line[1]))
                break

            line = fid.readline()
           


        fid.close()
        os.chdir('../')

    os.chdir('../')



    plot(betat1, betae1, 'bs-', linewidth=1.25, label='Ground State', markersize=8)
    plot(betat_ex, betae_ex, 'go-', linewidth=1.25, label='Excited State',
            markersize=8)

    print betat_ex1
    print betae_ex1
    plot(betat_ex1, betae_ex1, 'ro-', linewidth=1.25, label='Excited State',
            markersize=8)

    xlim([0,1.6])

    
    #plot([-1,0,1],[0,1,0],'gs-', linewidth=1.25, markersize=8)#label=r'$\forall \mathrm{Fock}$', markersize=8)

    xlabel(r'$\beta_{T}$', fontsize=24)
    ylabel(r'$\beta_{E}$', fontsize=24)


    #labels = ['APAGP','CPAGP','Checkerboard', 'Paramagnet']
    #dat_x = [-1.2,1.2 , 0, 0] 
    #dat_y = [-0.2,-0.2,-1, 0.333]

    #for label, x, y in zip(labels, dat_x,dat_y):
    #    annotate(label, xy = (x, y), xytext = (-20, 20), textcoords = 'offset points', ha =
    #    'center', va = 'bottom', bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
    #        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))


    #ylabel(r'$|\langle \hat{n}_{i,\alpha}\hat{n}_{i+R,\beta}\rangle - \langle\hat{n}_{\alpha}\rangle\langle\hat{n}_{\beta}\rangle|$',
    #        fontsize=24)
    #xlim([0,1.8])
    tick_params(labelsize=20)
    #xlabel(r'$U$', fontsize=28)
    
    tight_layout()
    legend(loc='upper right', frameon=False, prop={'size':16})

    print os.getcwd()
    savefig('test.pdf', format='PDF')
    show()
       
