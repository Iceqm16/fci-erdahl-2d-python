from pylab import *
import os
import sys
import unittest
rc('font', family="Times New Roman")

if __name__=="__main__":


    betat1 = []
    betae1 = []

    #betat.append(0)
    #betae.append(1)

    os.chdir(sys.argv[1])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat1.append(float(line[0]))
                betae1.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')


    betat2 = []
    betae2 = []

    os.chdir(sys.argv[2])
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        dirt[int(d.split('r')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat2.append(float(line[0]))
                betae2.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')
 
    os.chdir('../')
    
    betat3 = []
    betae3 = []

    os.chdir(sys.argv[3])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat3.append(float(line[0]))
                betae3.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')

    betat4 = []
    betae4 = []

    os.chdir(sys.argv[4])
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        dirt[int(d.split('r')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat4.append(float(line[0]))
                betae4.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')


    plot(betat1, betae1, 'bs-', linewidth=1.25, label=r'$ [\alpha_{T},\alpha_{E}] \in [-1,[-1,1]]$', markersize=8)
    plot(betat2, betae2, 'rs-', linewidth=1.25, label=r'$ [\alpha_{T},\alpha_{E}] \in [ [ -1,1],1]$', markersize=8)
    plot(betat3, betae3, 'ms-', linewidth=1.25, label=r'$ [\alpha_{T}, \alpha_{E} \in [ 1,[1,-1]]$', markersize=8)
    plot(betat4, betae4, 'ks-', linewidth=1.25, label=r'$[\alpha_{T},\alpha_{E} \in [ [-1,1],-1]$', markersize=8)
    #plot(betat5, betae5, 'go-', linewidth=1.25,markersize=8)# label=r'$\forall\; \mathrm{Fock}\; \alpha \in [ [-1,1],-1]$', markersize=8)


    
    #plot([-1,0,1],[0,1,0],'gs-', linewidth=1.25, markersize=8)#label=r'$\forall \mathrm{Fock}$', markersize=8)

    title(r'$|\Lambda| = 6 \; \langle n\rangle = \frac{2}{3}$')
    xlabel(r'$\beta_{T}$', fontsize=24)
    ylabel(r'$\beta_{E}$', fontsize=24)

    betat1 = []
    betae1 = []

    #betat.append(0)
    #betae.append(1)

    os.chdir(sys.argv[5])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat1.append(float(line[0]))
                betae1.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')


    betat2 = []
    betae2 = []

    os.chdir(sys.argv[6])
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        dirt[int(d.split('r')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat2.append(float(line[0]))
                betae2.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')
 
    os.chdir('../')
    
    betat3 = []
    betae3 = []

    os.chdir(sys.argv[7])
    dirs = filter(os.path.isdir, os.listdir('./'))
    for d in dirs:
        os.chdir(d)
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat3.append(float(line[0]))
                betae3.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')

    betat4 = []
    betae4 = []

    os.chdir(sys.argv[8])
    dirs = filter(os.path.isdir, os.listdir('./'))
    dirt = {}
    for d in dirs:
        dirt[int(d.split('r')[1])] = d
    for d in dirt.keys():
        os.chdir(dirt[d])
        fid = open('fci.out','r')
        line = fid.readline()
        while 'SUCCESS' not in line:
            
            if 'BETA\'S' in line:
                line = fid.readline()
                line = line.split('\t')
                betat4.append(float(line[0]))
                betae4.append(float(line[1]))
                break
            line = fid.readline()

        fid.close()
        os.chdir('../')

    os.chdir('../')


    plot(betat1, betae1, 'bs-', linewidth=1.25, label=r'$ [\alpha_{T},\alpha_{E}] \in [-1,[-1,1]]$', markersize=8)
    plot(betat2, betae2, 'rs-', linewidth=1.25, label=r'$ [\alpha_{T},\alpha_{E}] \in [ [ -1,1],1]$', markersize=8)
    plot(betat3, betae3, 'ms-', linewidth=1.25, label=r'$ [\alpha_{T}, \alpha_{E} \in [ 1,[1,-1]]$', markersize=8)
    plot(betat4, betae4, 'ks-', linewidth=1.25, label=r'$[\alpha_{T},\alpha_{E} \in [ [-1,1],-1]$', markersize=8)
    #plot(betat5, betae5, 'go-', linewidth=1.25,markersize=8)# label=r'$\forall\; \mathrm{Fock}\; \alpha \in [ [-1,1],-1]$', markersize=8)



    #labels = ['APAGP','CPAGP','Checkerboard', 'Paramagnet']
    #dat_x = [-1.2,1.2 , 0, 0] 
    #dat_y = [-0.2,-0.2,-1, 0.333]

    #for label, x, y in zip(labels, dat_x,dat_y):
    #    annotate(label, xy = (x, y), xytext = (-20, 20), textcoords = 'offset points', ha =
    #    'center', va = 'bottom', bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
    #        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))


    #ylabel(r'$|\langle \hat{n}_{i,\alpha}\hat{n}_{i+R,\beta}\rangle - \langle\hat{n}_{\alpha}\rangle\langle\hat{n}_{\beta}\rangle|$',
    #        fontsize=24)
    #ylim([-14,0])
    tick_params(labelsize=20)
    #xlabel(r'$U$', fontsize=28)
    
    tight_layout()
    legend(loc='upper right', frameon=False, prop={'size':16})

    print os.getcwd()
    savefig('test.pdf', format='PDF')
    show()
       
