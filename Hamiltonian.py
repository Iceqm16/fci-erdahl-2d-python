'''
Build the Hamiltonian given the basis string.

Builds a csr_matrix of the Hamiltonian to minimize memory requirements.
'''


from input import *
import BasisBuilder as BB
import global_fun as gf
import numpy as np
import time
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import eigsh
import sys
import matplotlib as ml
import matplotlib.pyplot as plt
import os
import pandas as pd


try:
     from numba import autojit
     AJIT = True
except ImportError, e:
    AJIT = False
    pass # module doesn't exist, deal with it.
    

class Hamiltonian:

    def __init__(self,Basis, fid):

        if ('T2.ham' in os.listdir('./')) and ('E2.ham' in os.listdir('./')):
            print "\t\tBUILDING HAMILTONIAN"
            fid.write("\t\tBUILDING HAMILTONIAN\n")
            start_time  = time.time()

            E2 = pd.read_csv("./E2.ham")
            T2 = pd.read_csv("./T2.ham")

            H_E2 = csr_matrix( (E2['val'], (E2['row'],E2['col'])),
                    shape=(len(Basis.LT),len(Basis.LT)) )
            H_T2 = csr_matrix( (T2['val'], (T2['row'],T2['col'])),
                    shape=(len(Basis.LT),len(Basis.LT)) )

            #print H_E2*alpha_e
            #print H_T2
            #sys.exit()
            Ham = H_E2*alpha_e + H_T2*alpha_t
            #csr_matrix( (dat, (row,col)), shape=(len(Basis.LT),len(Basis.LT)))
            self.Ham_Mat = Ham
            self.H_E2 = H_E2
            self.H_T2 = H_T2
 
        else:

            M = 2**L #THE BASIS NUMBER TO DETERMINE ALPHA/BETA STRINGS
            #Ham1 = np.zeros(( len(Basis.LT), len(Basis.LT) ) )
            #Ham = csr_matrix( (len(Basis.LT), len(Basis.LT) ), dtype=float)
            global strings
            strings = Basis.strings
            row = []
            col = []
            dat = []

            rowe = []
            cole = []
            date = []

            rowt = []
            colt = []
            datt = []
            
            xcount = 0
            ycount = 0
            print "\t\tBUILDING HAMILTONIAN"
            fid.write("\t\tBUILDING HAMILTONIAN\n")
            start_time  = time.time()

            #ELINIMNATE DOTS SPEEDS UP FOR LOOPS
            #Hamiltonian = self.Hubbard_1D
            Hamiltonian = self.Erdahl_QPT
            rappend = row.append
            cappend = col.append
            dappend = dat.append

            E2 = self.Erdahl_QPT_E
            reappend = rowe.append
            ceappend = cole.append
            deappend = date.append

            T2 = self.Erdahl_QPT_T
            rtappend = rowt.append
            ctappend = colt.append
            dtappend = datt.append

            for xx in range(len(Basis.LT)):

                lax = Basis.LT[xx] / M
                lbx = Basis.LT[xx] % M
     
                for yy in range(xx,len(Basis.LT)):
                    #print xx
                    #print yy
                    #if xx == 5:
                    #    sys.exit()
                    lay = Basis.LT[yy] / M
                    lby = Basis.LT[yy] % M

                    #H_elem = Hamiltonian(lax,lbx,lay,lby, Basis.LT[xx],
                    #    Basis.LT[yy])
                    H_elem_E = E2(lax,lbx,lay,lby, Basis.LT[xx],
                        Basis.LT[yy])
                    H_elem_T = T2(lax,lbx,lay,lby, Basis.LT[xx],
                        Basis.LT[yy])

                    H_elem = H_elem_E*alpha_e + H_elem_T*alpha_t 
                    if H_elem != 0 and xx != yy:
                        rappend(xx)
                        cappend(yy)
                        dappend(H_elem)
                        rappend(yy)
                        cappend(xx)
                        dappend(H_elem)
                    if H_elem !=0 and xx == yy:
                        rappend(xx)
                        cappend(yy)
                        dappend(H_elem)


                    if H_elem_E != 0 and xx != yy:
                        #print "E [%i,%i] = %f" % (xx,yy,H_elem_E)
                        reappend(xx)
                        ceappend(yy)
                        deappend(H_elem_E)
                        reappend(yy)
                        ceappend(xx)
                        deappend(H_elem_E)
                    if H_elem_E !=0 and xx == yy:
                        reappend(xx)
                        ceappend(yy)
                        deappend(H_elem_E)

                    if H_elem_T != 0 and xx != yy:
                        #print "T [%i,%i] = %f" % (xx,yy,H_elem_T)
                        rtappend(xx)
                        ctappend(yy)
                        dtappend(H_elem_T)
                        rtappend(yy)
                        ctappend(xx)
                        dtappend(H_elem_T)
                    if H_elem_T !=0 and xx == yy:
                        rtappend(xx)
                        ctappend(yy)
                        dtappend(H_elem_T)



                        
            Ham = csr_matrix( (dat, (row,col)), shape=(len(Basis.LT),len(Basis.LT)))
            H_E2 = csr_matrix( (date, (rowe,cole)),
                    shape=(len(Basis.LT),len(Basis.LT)) )
            H_T2 = csr_matrix( (datt, (rowt,colt)),
                    shape=(len(Basis.LT),len(Basis.LT)) )


            fid.flush()
            self.Ham_Mat = Ham
            self.H_E2 = H_E2
            self.H_T2 = H_T2
            #w,v = eigsh(Ham,k=6,mode= 'buckling' ,which='SA')
            #print "\tGROUND STATE ENERGY FROM SPARSE = %f" % w[0]

            fidt = open('T2.ham','w')
            fidt.write('row,col,val\n')
            for xx in range(len(rowt)):
                fidt.write("%i,%i,%f\n"%(rowt[xx],colt[xx],datt[xx]))
            fidt.close()
            fidt = open('E2.ham','w')
            fidt.write('row,col,val\n')
            for xx in range(len(rowe)):
                fidt.write("%i,%i,%f\n"%(rowe[xx],cole[xx],date[xx]))
            fidt.close()
            fidt = open('Ham.ham','w')
            fidt.write('row,col,val\n')
            for xx in range(len(row)):
                fidt.write("%i,%i,%f\n"%(row[xx],col[xx],dat[xx]))
            fidt.close()




        print "\t\tTIME TO BUILD HAMILTONIAN = %f\n" % (time.time() -\
                start_time)
        fid.write("\t\tTIME TO BUILD HAMILTONIAN = %f\n" % (time.time() -\
                start_time) )


        #Ham = Ham.todense()
        #ww,vv = np.linalg.eigh(Ham)
        #sorted(ww,reverse=True)

        #print "\tGROUND STATE ENERGY = %f" %  ww[0]
        
        #plt.imshow(Ham)
        #plt.colorbar(orientation='vertical')
        #plt.show()
        #Tmat = Ham - Ham1
        #for xx in range(Tmat.shape[0]):
        #    for yy in range(Tmat.shape[1]):
        #        if Tmat[xx,yy] != 0:
        #            print Tmat[xx,yy]



    def Erdahl_QPT_T(self, Ia_bra, Ib_bra, Ia_ket, Ib_ket, Ibra, Iket):

        #ELEMENT VALUE TO BE RETURNED AND VARIABLE DEFINITION
        H_elem = 0
        sign1 = 1
        sign2 = 1
        sign3 = 1
        sign4 = 1

        #PAIR LIST OPERATIONS

        #_, ket_a = gf.DectoBin(Na,Ia_ket)
        #_, bra_a = gf.DectoBin(Na,Ia_bra)
        #_, ket_b = gf.DectoBin(Nb,Ib_ket)
        #_, bra_b = gf.DectoBin(Nb,Ib_bra)
        ket_a = strings[Ia_ket]
        bra_a = strings[Ia_bra]
        ket_b = strings[Ib_ket]
        bra_b = strings[Ib_bra]
        #TWO BODY PART OF HAMILTONIAN
       
        #HAMILTONIAN ACTS ON KET AND CHECKS BRA.  LOOP OVER PAIRS ON LATTICE
        nnpairs = []
        Lt = int(np.sqrt(L))
        tmp = map(lambda x: [x%Lt,(x+1)%Lt] , range(L))
        tmp = map(lambda y: map(lambda x: [x%Lt+(y*Lt), (x+1)%Lt + (y*Lt)],
            range(Lt)), range(Lt))
        tmp = [item for sublist in tmp for item in sublist]

        tmp2 = map(lambda y: map(lambda x: [ ((x%Lt) + y*Lt)%L, ((x+Lt)%L +
            y*Lt)%L], range(Lt)) , range(Lt))

        tmp2 = [item for sublist in tmp2 for item in sublist]


        for t in tmp+tmp2:
            nnpairs.append(t)



        for p in nnpairs:
            #TRANSPORT OF PAIRS
            ket_a_t = list(ket_a)
            ket_b_t = list(ket_b)
            goodKub, sign1 = gf.Killsign(p[0], ket_b_t)
            goodKua, sign2 = gf.Killsign(p[0], ket_a_t)
            goodCla, sign3 = gf.Createsign(p[1], ket_a_t)
            goodClb, sign4 = gf.Createsign(p[1], ket_b_t)

            if goodKub and goodKua and goodCla and goodClb:
                Ib_ket_t = gf.ItfromVec(ket_b_t)
                Ia_ket_t = gf.ItfromVec(ket_a_t)
                Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                if Iket_t == Ibra:
                    H_elem += 2.#*alpha_t


            ket_a_t = list(ket_a)
            ket_b_t = list(ket_b)
            goodKub, sign1 = gf.Killsign(p[1], ket_b_t)
            goodKua, sign2 = gf.Killsign(p[1], ket_a_t)
            goodCla, sign3 = gf.Createsign(p[0], ket_a_t)
            goodClb, sign4 = gf.Createsign(p[0], ket_b_t)
            if goodKub and goodKua and goodCla and goodClb:
                Ib_ket_t = gf.ItfromVec(ket_b_t)
                Ia_ket_t = gf.ItfromVec(ket_a_t)
                Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                if Iket_t == Ibra:
                    H_elem += 2.#*alpha_t
 
        return H_elem 
 

    def Erdahl_QPT_E(self, Ia_bra, Ib_bra, Ia_ket, Ib_ket, Ibra, Iket):

        #ELEMENT VALUE TO BE RETURNED AND VARIABLE DEFINITION
        H_elem = 0
        sign1 = 1
        sign2 = 1
        sign3 = 1
        sign4 = 1

        #PAIR LIST OPERATIONS

        #_, ket_a = gf.DectoBin(Na,Ia_ket)
        #_, bra_a = gf.DectoBin(Na,Ia_bra)
        #_, ket_b = gf.DectoBin(Nb,Ib_ket)
        #_, bra_b = gf.DectoBin(Nb,Ib_bra)
        ket_a = strings[Ia_ket]
        bra_a = strings[Ia_bra]
        ket_b = strings[Ib_ket]
        bra_b = strings[Ib_bra]
        
        #TWO BODY PART OF HAMILTONIAN
       
        #HAMILTONIAN ACTS ON KET AND CHECKS BRA.  LOOP OVER PAIRS ON LATTICE
        nnpairs = []
        Lt = int(np.sqrt(L))
        tmp = map(lambda x: [x%Lt,(x+1)%Lt] , range(L))
        tmp = map(lambda y: map(lambda x: [x%Lt+(y*Lt), (x+1)%Lt + (y*Lt)],
            range(Lt)), range(Lt))
        tmp = [item for sublist in tmp for item in sublist]

        tmp2 = map(lambda y: map(lambda x: [ ((x%Lt) + y*Lt)%L, ((x+Lt)%L +
            y*Lt)%L], range(Lt)) , range(Lt))

        tmp2 = [item for sublist in tmp2 for item in sublist]

        for t in tmp+tmp2:
            nnpairs.append(t)
            #nnpairs.append([t[1],t[0]])
  

        if Iket == Ibra:
            #print "state = %s|%s" % ("".join(str(x) for x in ket_a), "".join(str(x)
            #    for x in ket_b))
            pass


        for p in nnpairs:
            
        
            if Iket == Ibra:
                ket_a_t = list(ket_a)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25#sign1*sign2*sign3*sign4*alpha_e

                #E1 AlalauAu
                ket_a_t = list(ket_a)
                goodCu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25#*sign1*sign2*sign3*sign4*alpha_e

                #E1 alAlAuau
                ket_a_t = list(ket_a)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25#*sign1*sign2*sign3*sign4*alpha_e

                #E1 alAlauAu
                ket_a_t = list(ket_a)
                goodCu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25

                
                #2 BlblBubu - BlblbuBu - blBlBubu + blBlbuBu
                #E2 BlblBubu
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25

                #E2 BlblbuBu
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25

                #E2 alBlBubu
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25

                #E2 blBlbuBu
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25
                      
    
                #E3 AlalBubu - AlalbuBu - alAlBubu + alAlbuBu
                #E3 AlalBubu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25
 
                #E3 AlalbuBu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25
    
                #E3 alAlBubu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25
 
                #E3 alAlbuBu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25
            
                #E4 BlblAuau - BlblauAu - blBlAuau + blBlauAu
                #E4 BlblAuau
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25
          
                #E4 BlblauAu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25
 
                #E4 blBlAuau        
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25
            
                #E4 blBlauAu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25


                #print "acting pair [%i,%i] = %f" % (p[0],p[1], -1.*H_elem)
 
        return H_elem 
 

    def Erdahl_QPT(self, Ia_bra, Ib_bra, Ia_ket, Ib_ket, Ibra, Iket):

        #ELEMENT VALUE TO BE RETURNED AND VARIABLE DEFINITION
        H_elem = 0
        sign1 = 1
        sign2 = 1
        sign3 = 1
        sign4 = 1

        #PAIR LIST OPERATIONS

        _, ket_a = gf.DectoBin(Na,Ia_ket)
        _, bra_a = gf.DectoBin(Na,Ia_bra)
        _, ket_b = gf.DectoBin(Nb,Ib_ket)
        _, bra_b = gf.DectoBin(Nb,Ib_bra)
        
        #TWO BODY PART OF HAMILTONIAN
       
        #HAMILTONIAN ACTS ON KET AND CHECKS BRA.  LOOP OVER PAIRS ON LATTICE
        nnpairs = []
        tmp = map(lambda x: [x%L,(x+1)%L] , range(L))
        for t in tmp:
            nnpairs.append(t)
            #nnpairs.append([t[1],t[0]])
     

        for p in nnpairs:

            #TRANSPORT OF PAIRS
            ket_a_t = list(ket_a)
            ket_b_t = list(ket_b)
            goodKub, sign1 = gf.Killsign(p[0], ket_b_t)
            goodKua, sign2 = gf.Killsign(p[0], ket_a_t)
            goodCla, sign3 = gf.Createsign(p[1], ket_a_t)
            goodClb, sign4 = gf.Createsign(p[1], ket_b_t)

            #print "".join(str(x) for x in ket_a) + "|" + "".join(str(x) for x
            #        in ket_b) +"\tto\t" + "".join(str(x) for x in ket_a_t) +\
            #                "|" + "".join(str(x) for x in ket_b_t) + \
            #                "\t[%i,%i]" % (p[0], p[1]) + \
            #        "\tBra\t" + "".join(str(x) for x in bra_a) + "|" + \
            #        "".join(str(x) for x in bra_b)
            #print "%i,%i,%i,%i" % (goodKub, goodKua, goodCla, goodClb)
            if goodKub and goodKua and goodCla and goodClb:
                Ib_ket_t = gf.ItfromVec(ket_b_t)
                Ia_ket_t = gf.ItfromVec(ket_a_t)
                Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                if Iket_t == Ibra:
                    H_elem += 2.*alpha_t
                    #print "".join(str(x) for x in ket_a) + "|" +\
                    #        "".join(str(x) for x in ket_b) + "\ttransport\t" +\
                    #       "".join(str(x) for x in ket_a_t) + "|" +\
                    #        "".join(str(x) for x in ket_b_t) + \
                    #        "\t%f" % H_elem + "\t[%i,%i]" % (p[0],p[1])

            ket_a_t = list(ket_a)
            ket_b_t = list(ket_b)
            goodKub, sign1 = gf.Killsign(p[1], ket_b_t)
            goodKua, sign2 = gf.Killsign(p[1], ket_a_t)
            goodCla, sign3 = gf.Createsign(p[0], ket_a_t)
            goodClb, sign4 = gf.Createsign(p[0], ket_b_t)
            #print "".join(str(x) for x in ket_a) + "|" + "".join(str(x) for x
            #        in ket_b) +"\tto\t" + "".join(str(x) for x in ket_a_t) +\
            #                "|" + "".join(str(x) for x in ket_b_t) + \
            #                "\t[%i,%i]" % (p[0], p[1]) + \
            #        "\tBra\t" + "".join(str(x) for x in bra_a) + "|" + \
            #        "".join(str(x) for x in bra_b)
            #print "%i,%i,%i,%i" % (goodKub, goodKua, goodCla, goodClb)
            if goodKub and goodKua and goodCla and goodClb:
                Ib_ket_t = gf.ItfromVec(ket_b_t)
                Ia_ket_t = gf.ItfromVec(ket_a_t)
                Iket_t = (2**L)*Ib_ket_t + Ia_ket_t
                if Iket_t == Ibra:
                    H_elem += 2.*alpha_t
                    #print "".join(str(x) for x in ket_a) + "|" +\
                    #        "".join(str(x) for x in ket_b) + "\tctransport\t" +\
                    #       "".join(str(x) for x in ket_a_t) + "|" +\
                    #        "".join(str(x) for x in ket_b_t) + \
                    #        "\t%f" % H_elem + "\t[%i,%i]" % (p[0],p[1])


            if Iket == Ibra:
                #E1 AlalAuau - AlalauAu - alAlAuau + alAlauAu
                #E1 AlalAuau

                #print "".join(str(x) for x in ket_a) + "|" + "".join(str(x) for x
                #        in ket_b) +"\tto\t" + "".join(str(x) for x in ket_a_t) +\
                #                "|" + "".join(str(x) for x in ket_b_t) + \
                #                "\t[%i,%i]" % (p[0], p[1]) + \
                #        "\tBra\t" + "".join(str(x) for x in bra_a) + "|" + \
                #        "".join(str(x) for x in bra_b)

                ket_a_t = list(ket_a)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" +\
                    #        "".join(str(x) for x in ket_b) + "\tpa-pa" + \
                    #        "\t%f" % H_elem
                #E1 AlalauAu
                ket_a_t = list(ket_a)
                goodCu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25*alpha_e#*sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tha-pa" + \
                    #        "\t%f" % H_elem

                #E1 alAlAuau
                ket_a_t = list(ket_a)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25*alpha_e#*sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpa-ha" + \
                    #        "\t%f" % H_elem

                #E1 alAlauAu
                ket_a_t = list(ket_a)
                goodCu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_a_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25*sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tha-ha" + \
                    #        "\t%f" % H_elem

                
                #2 BlblBubu - BlblbuBu - blBlBubu + blBlbuBu
                #E2 BlblBubu
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25*sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpb-pb" + \
                    #        "\t%f" % H_elem

                #E2 BlblbuBu
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25*alpha_e#*sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\thb-pb" + \
                    #        "\t%f" % H_elem

                #E2 alBlBubu
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem -= 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpb-hb" + \
                    #        "\t%f" % H_elem

                #E2 blBlbuBu
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodKl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKu and goodKl and goodCu and goodCl:
                    H_elem += 0.25*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\thb-hb" + \
                    #        "\t%f" % H_elem
                      
    
                #E3 AlalBubu - AlalbuBu - alAlBubu + alAlbuBu
                #E3 AlalBubu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpb-pa" + \
                    #        "\t%f" % H_elem
 
                #E3 AlalbuBu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_a_t)
                goodCl, sign4 = gf.Createsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\thb-pa" + \
                    #        "\t%f" % H_elem
    
                #E3 alAlBubu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_b_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpb-ha" + \
                    #        "\t%f" % H_elem
 
                #E3 alAlbuBu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodCu, sign1 = gf.Createsign(p[0], ket_b_t)
                goodKu, sign2 = gf.Killsign(p[0], ket_b_t)
                goodCl, sign3 = gf.Createsign(p[1], ket_a_t)
                goodKl, sign4 = gf.Killsign(p[1],ket_a_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\thb-ha" + \
                    #        "\t%f" % H_elem
            
                #E4 BlblAuau - BlblauAu - blBlAuau + blBlauAu
                #E4 BlblAuau
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpa-pb" + \
                    #        "\t%f" % H_elem
          
                #E4 BlblauAu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Killsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Createsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tha-pb" + \
                    #        "\t%f" % H_elem
 
                #E4 blBlAuau        
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Killsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Createsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem -= 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tpa-hb" + \
                    #        "\t%f" % H_elem
            
                #E4 blBlauAu
                ket_a_t = list(ket_a)
                ket_b_t = list(ket_b)
                goodKu, sign1 = gf.Createsign(p[0], ket_a_t)
                goodCu, sign2 = gf.Killsign(p[0], ket_a_t)
                goodKl, sign3 = gf.Createsign(p[1], ket_b_t)
                goodCl, sign4 = gf.Killsign(p[1], ket_b_t)
                if goodKl and goodKu and goodCl and goodCu:
                    H_elem += 0.25*alpha_e#sign1*sign2*sign3*sign4*alpha_e
                    #print "".join(str(x) for x in ket_a) + "|" + \
                    #        "".join(str(x) for x in ket_b) + "\tha-hb" + \
                    #        "\t%f" % H_elem
 



 
        return H_elem 
 
    def Hubbard_1D(self, Ia_ket, Ib_ket, Ia_bra, Ib_bra, Iket, Ibra):

        #ELEMENT VALUE TO BE RETURNED AND VARIABLE DEFINITION
        H_elem = 0
        sign1 = 1
        sign2 = 1

        #PAIR LIST OPERATIONS

        _, ket_a = gf.DectoBin(Na,Ia_ket)
        #_, bra_a = gf.DectoBin(Na,Ia_bra)
        _, ket_b = gf.DectoBin(Nb,Ib_ket)
        #_, bra_b = gf.DectoBin(Nb,Ib_bra)

        #TWO BODY PART OF HAMILTONIAN
        if (Ia_ket == Ia_bra) and (Ib_ket == Ib_bra):

            for xx in range(len(ket_a)):

                if (ket_a[xx] == ket_b[xx]) and (ket_a[xx] == 1):

                    H_elem += U


        #ONE BODY PART OF HAMILTONIAN  sum_{<i,j>} c_{i}^{t}c_{j} | alpha,beta>

        #if Iket != Ibra:
        #    pass
            #print "State"
            #print "%s%s|%s%s" % ("".join(str(x) for x in bra_a), 
            #        "".join(str(x) for x in bra_b),
            #        "".join(str(x) for x in ket_a), 
            #        "".join(str(x) for x in ket_b) )


        for k in range(L):

            if ( k == 0):
                c = L -1
                
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK,sign1 = gf.Killsign(k, ket_a_t)
                goodC,sign2 = gf.Createsign(c, ket_a_t)

                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

                ket_b_t = list(ket_b)
                goodK,sign1 = gf.Killsign(k, ket_b_t)
                goodC,sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

            if k == (L - 1):
                c = 0
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK,sign1 = gf.Killsign(k, ket_a_t)
                goodC,sign2 = gf.Createsign(c, ket_a_t)

                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

                ket_b_t = list(ket_b)
                goodK, sign1 = gf.Killsign(k, ket_b_t)
                goodC, sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

            if k < (L - 1):
                c = k + 1
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK, sign1 = gf.Killsign(k, ket_a_t)
                goodC, sign2 = gf.Createsign(c, ket_a_t)

                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

                ket_b_t = list(ket_b)
                goodK, sign1 = gf.Killsign(k, ket_b_t)
                goodC, sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2

            if k > 0:
                c = k - 1
                ket_a_t = list(ket_a) #TRUE COPY USE list() or a[:]
                goodK,sign1 = gf.Killsign(k, ket_a_t)
                goodC,sign2 = gf.Createsign(c, ket_a_t)
               
                if goodK and goodC:

                    Ia_ket_t = gf.ItfromVec(ket_a_t)
                    Iket_t = (2**L)*Ib_ket + Ia_ket_t

                    if Iket_t == Ibra:
                        #print H_elem
                        H_elem += -1.*t*sign1*sign2
                        #print "moving alpha back one"
                        #print H_elem
 


                ket_b_t = list(ket_b)
                goodK,sign1 = gf.Killsign(k, ket_b_t)
                goodC,sign2 = gf.Createsign(c, ket_b_t)

                if goodK and goodC:

                    Ib_ket_t = gf.ItfromVec(ket_b_t)
                    Iket_t = (2**L)*Ib_ket_t + Ia_ket

                    if Iket_t == Ibra:

                        H_elem -= t*sign1*sign2


        return H_elem




if __name__=="__main__":

    fid = open('test','w')
    Basis_states = BB.Basis(4**L,Na,Nb, fid)

    Hamiltonian(Basis_states,fid)

    fid.close()
