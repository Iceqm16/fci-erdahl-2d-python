'''
Extract ground state and first excited state from fci.out in active directory.
Look at the energy gap as a function of maximal eigenvalue in the
2-RDM/Eigenvalue distribution.
'''


import os
import sys
import numpy 
from pylab import *
import unittest
rc('font', family="Times New Roman")



class CollectS0S1:

    def __init__(self,):

        dirs = filter(os.path.isdir, os.listdir('./'))
        GS = []
        ES = []
        alpha = [] #LIST OF TUPLES

        dirt = {}
        for d in dirs:
            dirt[int(d.split('r')[1])] = d

        print dirs
        print dirt.keys()
        for dd in dirt.keys():

            os.chdir(dirt[dd])
            Eigs = []
            print os.getcwd()
            fid = open('fci.out','r')
            line = fid.readline()


            while('EIGENVALUES' not in line): #ADVANCE TO eigenvalue line
                if 'alpha_t' in line:
                    alpha.append((float(line.split('=')[1]),
                            float(fid.readline().split('=')[1])))
                    print alpha[-1]
                line = fid.readline()

            while('NONZERO' not in line):
                line = fid.readline()
                print line.strip()
                try:
                    Eigs.append(float(line.strip()))
                except ValueError:
                    break

            GS.append(Eigs[0])

            ff = False
            for xx in Eigs[1:]:
                if xx !=  Eigs[0]:
                    ES.append(xx)
                    ff = True
                    break

            if not ff:
                ES.append(Eigs[0])

            fid.close()
            os.chdir('../')

        os.chdir('../')
        self.GS = GS
        self.ES = ES
        self.alpha = alpha


class PlotStates:

    def __init__(self,GS, ES, alpha):

        axes = figure().add_subplot(111)    
        plot(range(len(ES)),ES,'bs-',linewidth=1.25, label='Excited State')
        plot(range(len(GS)),GS,'ro-',linewidth=1.25,label='Ground State')

        xlabel(r'$\alpha$',fontsize=24)
        ylabel(r'$E$',fontsize=24)
        tick_params(labelsize=10)
        tight_layout()
        legend(loc='upper right', frameon=False, prop={'size':18})
        a=axes.get_xticks().tolist()

        labels = ['(0,-1)','(-1,-1)','(-1,1)','(-1,0.11)','(0,1)']#'CPAGP','Checkerboard', 'Paramagnet']
        dat_x = [0,9,18,14,27]#0,1.2 , 0, 0] 
        dat_y = [-6,-6,-11.3,-8.32,-6]#0,-0.2,-1, 0.333]

        #for label, x, y in zip(labels, dat_x,dat_y):
        #    annotate(label, xy = (x, y), xytext = (x,y-1),
        #    #textcoords = 'offset points', ha = 'center', va = 'bottom',
        #    bbox = dict(boxstyle = 'round,pad=0.5', fc = 'yellow', alpha = 0.5),
        #    arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))



        #xlim([0,38])
        savefig('test.pdf',format='PDF')
        show()



if __name__=="__main__":

    if len(sys.argv) != 2:
        print "Only one active directoyr required."
        sys.exit()

    print "moving to active directory"
    os.chdir(sys.argv[1])

    S0S1 = CollectS0S1()
    PlotStates(S0S1.GS, S0S1.ES, S0S1.alpha)
