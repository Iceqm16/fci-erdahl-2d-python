'''
Builds basis set for close shell system using strings of alpha and beta
electrons on a lattice.


For 2-D the basis states should probably stay the same.  The number will
correspond to electronic configurations that are different.  For example for
the 4x4 lattice the numbering should be

01 02 03 04
05 06 07 08
09 10 11 12
13 14 15 16

with sites 1-4, 5-8, 9-12, 13-16, 
1-13, 2-14, 3-15, 4-16 are connected for periodic boundary condition.

The permutations occupations over the lattice are the same.  Therefore, in
FCIrun we should enforce that the L is a square lattice.

'''
import global_fun as gf
from itertools import product
import time
import sys
sys.path.append('/home/nick/pyncr/ED')
from input import *
try:
     from numba import autojit
     AJIT = True
except ImportError, e:
    AJIT = False
    pass # module doesn't exist, deal with it.
 

class Basis:


    def __init__(self, nstate, Na, Nb, fid):

        
        self.Na = Na
        self.Nb = Nb
        self.fid = fid
        M = 2**L
        if ConservedN:
            #LT = self.BuildBasis()
            LT = self.BuildBasis_Choose()
            strings = {}
            for xx in LT:
                lax = xx % M
                lbx = xx / M
                _, ket_a = gf.DectoBin(Na,lax)
                _, ket_b = gf.DectoBin(Nb,lbx)
                strings[lbx] = ket_b
                strings[lax] = ket_a
            self.strings = strings
        elif NaNb:
            LT = self.BuildBasis_NaNb()
        elif Ionic_Valence:
            LT = self.BuildBasisIonicValence()
        else:
            LT = self.BuildBasis_Fock()
        self.LT = LT
 
    def BuildBasisIonicValence(self,):

        #BUILD ENTIRE FOCK SPACE FULL 4**L VECTORS STARTING WITH STRINGS OF
        #ZERO [0]*L to FULLY OCCUPIED STRINGS!  THEN SORT BY NUMBER OF
        #PARTICLES AND SPIN COMPONENT. OR DON'T SORT AND JUST DIAGONALIZE.

        min_a = 0
        max_a = gf.MaxI(L)
        min_b = 0
        max_b = gf.MaxI(L)
        #INTEGER BASIS HOLDER
        LT = []
        M = 2**L
    
        start_time = time.time()
        for i in range(min_a, max_a+1):
            bin_ = gf.DectoBin_Fock(Na, i)

            for j in range(min_b,max_b+1):
                bin_t = gf.DectoBin_Fock(Nb,j)
                if ( sum(map(lambda x: bin_[x] == bin_t[x] and bin_[x] == 1,
                    range(L))) == Ni):

                    if sum(map(lambda x: bin_[x] != bin_t[x] and (bin_[x] == 1 or
                        bin_t[x] == 1), range(L) )) == Nv:
                        #print "%s\t%i" % ("".join(str(x) for x in bin_) + '|' + \
                        #"".join(str(x) for x in bin_t) , sum(bin_) + sum(bin_t))
                        LT.append( int( ( 2**L)*i + j ) )


                #print "i = %i\ti_c = %i\tstate = %i" % (i , LT[-1] / M, LT[-1])
                #print "j = %i\tj_c = %i\t" % (j, LT[-1] % M)

        print "\t\tNUMBER OF STRINGS = %f" % len(LT)
        print "\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
        self.fid.write("\t\tNUMBER OF STRINGS = %f\n" % len(LT) )
        self.fid.write("\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
                )


        sorted(LT)
        fid.flush()

        return LT

   
    
    def BuildBasis_Fock(self,):

        #BUILD ENTIRE FOCK SPACE FULL 4**L VECTORS STARTING WITH STRINGS OF
        #ZERO [0]*L to FULLY OCCUPIED STRINGS!  THEN SORT BY NUMBER OF
        #PARTICLES AND SPIN COMPONENT. OR DON'T SORT AND JUST DIAGONALIZE.

        min_a = 0
        max_a = gf.MaxI(L)
        min_b = 0
        max_b = gf.MaxI(L)
        #INTEGER BASIS HOLDER
        LT = []
        M = 2**L
    
        start_time = time.time()
        for i in range(min_a, max_a+1):
            bin_ = gf.DectoBin_Fock(Na, i)

            for j in range(min_b,max_b+1):
                bin_t = gf.DectoBin_Fock(Nb,j)
                #print "%s" % ("".join(str(x) for x in bin_) + '|' + \
                #        "".join(str(x) for x in bin_t) )
                LT.append( int( ( 2**L)*i + j ) )


                #print "i = %i\ti_c = %i\tstate = %i" % (i , LT[-1] / M, LT[-1])
                #print "j = %i\tj_c = %i\t" % (j, LT[-1] % M)

        print "\t\tNUMBER OF STRINGS = %f" % len(LT)
        print "\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
        self.fid.write("\t\tNUMBER OF STRINGS = %f\n" % len(LT) )
        self.fid.write("\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
                )


        sorted(LT)

        return LT

    def BuildBasis_NaNb(self,):

        #BUILD HALF FILLED STATES OF DIFFERENT MAGNETISM
        

        min_a = 0
        max_a = gf.MaxI(L)
        min_b = 0
        max_b = gf.MaxI(L)
        #INTEGER BASIS HOLDER
        LT = []
        M = 2**L
    
        start_time = time.time()
        for i in range(min_a, max_a+1):
            bin_ = gf.DectoBin_Fock(Na, i)
            #bin_ = map(lambda x: int(bin_[x]), range(len(bin_)))
            for j in range(min_b,max_b+1):
                bin_t = gf.DectoBin_Fock(Nb,j)
                #bin_t = map(lambda x: int(bin_t[x]), range(len(bin_t)))
               
                if (sum(bin_) + sum(bin_t) == N):
             
                    #print "%s\t%i" % ("".join(str(x) for x in bin_) + '|' + \
                    #    "".join(str(x) for x in bin_t), sum(bin_) + sum(bin_t) )
    
                    LT.append( int( ( 2**L)*i + j ) )


                #print "i = %i\ti_c = %i\tstate = %i" % (i , LT[-1] / M, LT[-1])
                #print "j = %i\tj_c = %i\t" % (j, LT[-1] % M)

        print "\t\tNUMBER OF STRINGS = %f" % len(LT)
        print "\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
        self.fid.write("\t\tNUMBER OF STRINGS = %f\n" % len(LT) )
        self.fid.write("\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)
                )


        sorted(LT)

        return LT

    def BuildBasis_Choose(self,):

        '''
        Store all of the N choose Na vectors for close shell.  Join together
        with itself forming (N choose Na)**2 basis and convert to integer.
        '''
        
        min_a = gf.MinI(self.Na)
        max_a = gf.MaxI(self.Na)

        LT = []


        print "\t\tBUILDING BASIS\n"
        print "\t\tSTARTING FROM MIN_A: %f" % min_a
        print "\t\tENDING AT MAX_A: %f" % max_a
        
        start_time = time.time()
        for i in xrange(min_a,max_a+1):

            la_good, bin_1 = gf.DectoBin(Na,i)
                
            if (la_good == 1):
                    LT.append( int( (2**L)*i + i) )

        print "\t\tNUMBER OF STRINGS = %f" % len(LT)
        print "\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)

        #BUILD INTEGER REPRESENTATION OF EACH POSSIBLE STATE AND SORT!
        sorted(LT)
          
        return LT
 

    #@autojit
    def BuildBasis(self,):

        min_a = gf.MinI(self.Na)
        min_b = gf.MinI(self.Nb)
        max_a = gf.MaxI(self.Na)
        max_b = gf.MaxI(self.Nb)

        LT = []


        print "\t\tBUILDING BASIS\n"
        print "\t\tSTARTING FROM MIN_A: %f" % min_a
        print "\t\tENDING AT MAX_A: %f" % max_a
        
        start_time = time.time()
        for i in xrange(min_a,max_a+1):

            la_good, bin_1 = gf.DectoBin(Na,i)

            for j in xrange(min_b,max_b+1):

                lb_good, _ = gf.DectoBin(Nb,j)
                
                if (lb_good == la_good and la_good == 1):
                    LT.append( int( (2**L)*i + j) )

        print "\t\tNUMBER OF STRINGS = %f" % len(LT)
        print "\t\tTIME TO BUILD BASIS = %f\n" % (time.time() - start_time)

        #BUILD INTEGER REPRESENTATION OF EACH POSSIBLE STATE AND SORT!
        sorted(LT)
          
        return LT
                    

if __name__=="__main__":

    nn = 4**6
  
    fid = open('test.txt','w')
    T = Basis(nn,Na,Nb,fid)
    fid.close()


    
