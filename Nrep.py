'''
Calculate N-representable region given FCI ground state vector and the
Hamiltonian


'''
import numpy as np
from input import *
from scipy.sparse import csr_matrix
import sys

class Nrep:

    def __init__(self, Diag, HH, fid):

        v = Diag.eigenvectors
        w = Diag.eigenvalues
        
        DN = csr_matrix(np.outer(v[:,0],v[:,0]))
        T2 = HH.H_T2
        E2 = HH.H_E2
        beta = [ np.sum(DN.dot(T2).diagonal())/L , np.sum(DN.dot(E2).diagonal()
            )/L ]

        fid.write("\nBETA'S\n")
        fid.write("%f\t%f\n"% (beta[0], beta[1]) )
   
        if v.shape[1] > 1:
            DN2 = csr_matrix(np.outer(v[:,1],v[:,1]))
            beta2 = [ np.sum(DN2.dot(T2).diagonal())/L , np.sum(DN2.dot(E2).diagonal()
            )/L ]
            fid.write("\nBETA'S 2\n")
            fid.write("%f\t%f\n"% (beta2[0], beta2[1]) )
 

        if v.shape[1] > 2:
            DN3 = csr_matrix(np.outer(v[:,2],v[:,2]))
            beta3 = [ np.sum(DN3.dot(T2).diagonal())/L , np.sum(DN3.dot(E2).diagonal()
            )/L ]

            fid.write("\nBETA'S 3\n")
            fid.write("%f\t%f\n"% (beta3[0], beta3[1]) )



        #print "beta2"
        #print "todense = %f" % (np.trace(DN2.dot(T2).todense())/L)
        #print "csr = %f" % beta2[0]
        #print "beta3"
        #print "todense = %f" % (np.trace(DN3.dot(T2).todense())/L)
        #print "csr = %f" % beta3[0]




